**Blackstone's Legacy - a Blackstone Fortress campaign for Kill Teams**

Skip sections/chapters/entries marked as "TODO" (they are "to do" so not ready yet)!

- Author: r4dh4l (r4dh4l-bslegacy@riseup.net)
- License: All parts of this text not quoting directly or indirectly from copyright material are licensed under [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) (this is all material mentioning GW sources in designer's note at the end of the section).

# Background story

Since [a new Blackstone Fortress appeared](https://invidio.us/watch?v=07bouWnhUL4) adventurers from the whole galaxy seek the treasures the unknown inside of the Fortress seems to promise. Soon there where rumors of a "Hidden Vault" inside the Blackstone Fortress after adventurers found datacubes which seem to be parts of a manual providing information how to access this special room inside the Fortress. All major factions send out Kill Teams to find this vault by all means: If the general ArcheoTech found in the Blackstone Fortress can already provide incredibly powerful technology how powerful would it be what is inside the Hidden Vault? Maybe the key to control the whole Blackstone Fortress?

# Campaign victory conditions

The first Kill Team managing to find 4 of the so called "Hidden Vault Datacubes" (HVD) will have a chance to disclose the secret of the Blackstone Fortress to win the main plot of the campaign.

# Rewards

## Main plot

All Command Overview registration fees will be taken to honour the campaign winner with a Games Workshop gift certificate (minus the money needed to replace general game material damages by lemonade or snacks).

## Side plots

### Arena of Precipice

All Arena participation fees will be divided by 2 at the end of the last campaign day and will be used to honour the Arena winner with a Games Workshop gift certificate: 

- 1 gift certificate for the player with the Kill Team which collected overall the most Arena Glory (team score)
- 1 gift certificate for the player of the unit which collected the most Arena Glory as lone unit (individual score).

# Things you need to play this campaign

## Rules / rule books

- "Warhammer 40,000: Kill Team Core Manual" (2018, updated by errata v2019-08-23), abbreviated by the wording "core rules" in this text.

- "Warhammer 40,000: Kill Team Commanders" (2018, updated by errata v2019-08-23)abbreviated by the wording "commander rules" in this text.

- "Warhammer 40,000: Kill Arena" (2019, updated by errata v2019-08-23), abbreviated by the wording "arena rules" in this text.

- "Warhammer 40,000: Kill Team Elites" (2019, updated by errata v2019-08-23), abbreviated by the wording "elite rules" in this text.

- "Warhammer 40,000: Kill Team Annual 2019" (2019), abbreviated by the wording "annual 2019" in this text.

- "Warhammer 40,000: Kill Team: Rogue Trader" (2018), abbreviated by the wording "rogue trader rules" in this text.

- White Dwarf June 2019 (the rules for playing Kill Team on Blackstone Fortress maps), abbreviated by the wording "KTonBSF rules" in this text.

- "Warhammer Quest Blackstone Fortress Datasheets for use in Games of Warhammer 40,000" (2018 - part of the Blackstone Fortress box), abbreviated by the wording "BSF40k rules" in this text.

## Materials

- For every "full" 2 participatns 1 map set of 1 "Warhammer Quest: Blackstone Fortress" (2008) box, example: 7 participants = 3 map sets.

- All "combat" and "combat-ambush" cards of 1 set "Warhammer Quest: Blackstone Fortress" (2008) "exploration" cards (you can take the whole set of exploration cards but the "challenge" cards will be ignored).

- 1 "Kill Team: Arena" (2019) box.

# Preparation

## Checklists (things you need to prepare before starting the campaign)

### As organizer / campaign leader

- You need at least 4 players for a diversified campaign experience. 2 or 3 players would technically work as well but won't provide a varied experience so less than 4 players should just be the exception if regular participants can not participate exceptionally. Initially this campaign was planned for 6 players. More than 8 will maybe become stressful if the organizer wants to participate as well because participatns will always ask the organizer in case of an ambigues situation.

- Prepare matching cards, each with the name of every participant. These pieces of paper should be equal in size and optic and will be needed to match the participants later on the battlefields.

- Prepare a registration box where participatns can place registrations for Arena matches via CMC. The box shall hide the CMC so that participants can not see which units will participate before matching the units.

- Discuss and decide if you want to play with 100p ("core units") or 125p ("Elite units") Battle-forget Kill Teams.

- Prepare units for Random Encounters (search for "Unit event" in section "The Fortress is alive: Random encounters!").

- Play some single test games on Blackstone Fortress maps to get a feeling for this kind of play.

- Decide which Blackstone Fortress maps you want to use during the campaign because there are some maps which would be really really difficult to play (start areas and corridors too small). You can find the designer's choice in `bslegacy-maps.jpeg`.

### As participant

1) Fee for registering your Command Overview (the intention is to prevent participants to set up new Kill Teams facing the first constraints in the campaign).

2) Fee for arena fights (something like 50 Cents per fight, intention is to have a 2nd price for a side plot).

3) Print the [Command Overview template](https://gitlab.com/r4dh4l/bslegacy/blob/master/templates/command-overview_2on1landscape.pdf) 1 time.

The print provides you an A4 sheet of paper with 2 pages on 1 in landscape format working as "Command Center" in paper format.

4) Print the [Curriculum Mortis Cards (CMC) template](https://gitlab.com/r4dh4l/bslegacy/blob/master/templates/cmc_back+front_4on1_print1on1doublesided.pdf) double-sided, mirrored on short edge (1 print provides CMCs for 4 units).

Each CMC provides you an enhanced data card. The front side provides all information you need *during* a mission/in battle, the back side provides everything need to log your charakter devlopment *after* a mission (injuries, experience). The intention is to have a "curriculum vitae" on the back side which tells you what you get for a certain level-up. The front side provides you all *current* information (because you can loose abilities you got by level-ups during the campaign).

5) Prepare your Kill Team as described in core rules pg203 for playing a campaign but:

- Ignore the "Fire Team" concept, each unit is a unit on its own.

- Use the printed "Command Overview" template instead of the usual "Command Roster".

- Prepare the Command Overview (depending on what the campaign organizer decided) for Battle-forged Kill Teams but with the regular maximum of 20 units (see next section which factions are allowed). **There is no unit limit for your Command Roster during the campaign from begin on** (so without the regular 12 unit start limit of the core rules pg205). The Command Overview template has just 30 slots because of layout reasons and can list even more but keep in mind that the more units you have on it the more you have to pay at the end of an ingame day to maintain your units (compare chapter "Pay maintenance costs for your Kill Team").
Attention: "List tailoring" is (at least in the beginning) not possible because you will never know which opponents you will meet in the Blackstone Fortress (in most of the cases, this will be random and only determined on which factions the other players will lead into the Fortress). So list tailoring has rather the function to provide you some replacement troops during the campaign (the chance is high that you loose your most favorite units during the campaign so play what you always wanted to play for fun, not only what you think is good to win). There are specialist abilities which will allow you to influence which opponent you will meet but you have to level up your units to use these abilities at first, of course.

- You should use a pencil to fill it out (you will need to modify values later).
Important: Don't use digital notes - this will break the game mechanic because you will need CMCs (physically on paper) at certain points during the campaign.

6) Write the CMC ID under the base of each of your units (the intention is to prevent discussions about identifying certain units while playing to save time). If you don't want ot use a permanent marker you can use small stickers for this (a hole puncher can create nice small stickers when you punch out part of the sticky part of a memo).

Designer's note: I didn't understand the 12 units limit because it is only set for the first match. Maybe the idea was to start with a smaller team which shall grow with the missions. After 2 test days all participatns decided to allow some reserve units especially for the beginning of the campaign so the 12 unit of the core rules pg205 limit was dropped. To limit the impact of a worst case start (loosing your best units after the first mission) our test players decided that it would be better not to have maintenance costs for your team (just) after the first ingame day so that you can start the campaign with some reserve units for the case of a bad start.

# Campaign rules

Unless otherwise noted in this text the campaign uses the core rules and elite rules.

## Allowed units and factions

All Non-COMMANDER units published officially by GW in any way until 2019-Oct-21 are allowed to use (including beta rules). As far as the author remembers these factions are:

- Core rules units (including the Adeptus Astartes updates in White Dwarf Oct 2019)
- trader units
- Elite rules units
- Kroot (presented in White Dwarf Feb 2019)
- Servants of the Abyss (including Escalation units for KT presented in White Dwarf Sep 2019)
- Blackstone Fortress Explorers
- Chaos Deamons (White Dwarf July 2019)

Designer's note: Although Tyarnids and "dark" Xenos factions are a problem in the whole story concept because you might think how and why they would live together with other factions on a space station: Let's imagine Precipice is a little bit mir like a space hulk or a Makropole where you have always parts which are not under full control of the local administration and as long things don't escalate too much the administration will accept some strange "roomers", won't they? The "Servants of the Abyss" are usually live in the Blackstone Fortress itself, of course but let's assume they have the same problems there to get supplies so the rules for other factions will fit there as well.
However, there are special rules here and there trying to respect special factions participating in the campaign to bring some problems down to a round figure.

## Ingame days

The campaign is played in ingame days. An ingame day is one rotation of Precipice around the Blackstone Fortress which is overall the time window a Kill Team can do all arrangements for a mission into the Blackstone Fortress, complete the mission, return back to Precipice, do all postprocessing like time for trading and Arena fights. An ingame day is around 4 realtime hours.

## Campaign duration

The campaign lasts 12 ingame days. The original idea planning this campaign was playing 6 campaign days, each with 2 ingame days over 6 months so 1 day per month playing 8 hours.

## For the organizer: Preparation

### Set up the battlefields

#### Blackstone Fortress battlefields

The campaign organizer has to prepare the campaign day by setting up battlefields in the Blackstone Fortress according to randomly chosen "combat" or "combat-ambush" Blackstone Fortress card.

If the amount of participants are even-numbered always 2 Kill Teams fight on 1 map.

If the amount of participants are off-numbered 3 randomly chosen teams of all participating teams will fight on 1 map together (yes, that maybe will lead into 2 against 1 but again: this campaign is about telling a story). All other teams will fight 1on1 on 1 map.

- The Maglev transport chamber counts as portal: If you don't have enough portal fields count the Maglev transport chamber as portal (which means that this portal offers more space to setup your units of chosen).

- Replace all discovery markers by objective markers: The objective marker will mark the middle of a hex field so that disctances from/to an objective marker will always be measured from the middle of the hex field.

#### Arena of Precipice

The campaign organizer has to prepare has to prepare 1 randomly chosen Kill Team Arena map representing the Arena of Precipice. This can be the same for the whole campaign day.

Important: **Don't set up doors!** The Arena shall provide fights, no hide-outs!

Place the Arena registration box next to the Arena board.

## Additional game rules

### Measing distances from/to objective markers

Objective markers are always in the middle of the hex field they were placed. So you don't need to care for the exact position of a marker. Just measure distances from the middle of the hex field (so not as ussual from the middle of the marker - this hopefully will save some time not debating about the exact position of an objective marker).

### Limits for Strategic Withdrawal

Strategical withdrawal (core rules pg203) is only allowed if 50 or more percent of your Kill Team units *counted in point value* has one or more flesh wounds and/or is out of action or more than the half of your units have one or more flesh wounds and/or is out of action.

Designer's note: This section is based on the Mordheim rules in Town Cryer 7 p2 ("The Editor speaks").

### Edging through bottlenecks

Because the campaign plays on Blackstone Fortress maps with very small corridors which would limit units with bases larger than 25mm the campaign uses the following rule for the case a model (according to the core rules) would not be able to pass a way because the it is too narrow:

If your base does not fit through an *environment* bottleneck you can try moving through it after passing a strength test (which is successful if you roll 1d6 less or equal of the strength value of the model that wants to pass the bottleneck) for every (partial) inch of the lenght of the narrow passage.
If the test is successfull you can pass the bottleneck handling the whole way as "Difficult Terrain" as described in core rules pg42 but with the difference that it affects models that can FLY as well.

If the unit doesn't pass one of the strength tests it stucks in the last completely past section.

Example: A unit with a 32mm base wants to pass a tunnel 25mm in width and 3,5" in lenght. The unit has to pass 4 strength tests, one for every (partial) inch. The unit doesn't pass the 4th streangth test so it stucks in the 3rd inch because it doesn't manage to pass the last, 4th inch.

If your base has the double size of the bottleneck (or is even larger) you have to treat the way as dangerous terrain in addition.

Example: A unit with a 50mm base wants to pass a tunnel 25mm in width and 3,5" in length. The unit has to pass 4 strength tests and has to roll 1d6 4 times to check for a mortal wound, one for every (partial) inch.

If your Movement value is not enough to pass the bottleneck you have to stop your movement directly in front of and in base contact to the environment which creates the bottleneck. The movement and the movement counts according to the type of movement you tried (advanced, charged etc.).

Keep in mind that this rule only works for environmental bottlenecks - it does not allow you to pass through units (your own or of your enemy) which form a bottleneck (yes, it can become very tricky to maneuver your units through the Blackstone Fortress and your own units can cause that one of your units is trapped between your own units and your opponent's one).

Designer's note: This rule was discussed in https://www.reddit.com/r/killteam/comments/d7r8h6/how_to_handle_model_base_limitations_on/f196p3n?utm_source=share&utm_medium=web2x.

### Line of Sight (LoS)

The line of sight (LoS) rules in the mix of the core rules and the rules mentioned in arena rules and KTonBSF rules can be confusing so this campaign uses the arena rules in the following interpretation:

If both bases of attacker and target are not in full cover LoS is considered from base points to base points (not from model to model).

If one or both of the bases is/are in full cover but not the model(s) standing on the base use any point of this model as start/end point for LoS.

### Psychologic effects

Some of the following rules mention psychologic effects which are not part of the core rules. These effects are explained in this section.

Designer's note: This sections is based on Mordheim rule book p23 ("Animosity" is not part of the original rule book but maybe was introduced with rules for Orks and Goblins).

#### FEAR

Fear is a natural reaction to huge or unnerving creatures. A model must take a Fear test by testing against the Leadership value with 2d6 in the following situations.

Note that units that cause fear can ignore these tests.

a) If the model is charged by unit which causes fear.

If a unit is charged by an enemy that he fears then he must take a test to overcome that fear.

Test when the charge is declared and is determined to be within range. If the test is passed the model may fight as normal.

If it is failed, the model must roll 6s to score hits in that round of combat.

b) If the model wishes to charge a fear-causing enemy.

If a unit wishes to charge an enemy that it fears then it must take a Ld test on 2d6 to overcome this. Treat this as a failed charge.

#### ANIMOSITY

At the start of your movement phase roll 1d6 for each unit with ANIMOSITY that is not in close combat. A result of `1` means that your unit has taken offense to something one of the other units of your Kill Team has done or said.
To find out find out how offended the model is, roll another 1d6 and consult the following chart to see what happens:

| 1d6 | Effect | Explanation |
| --- | --- | --- |
| 1 | "Take that!" | The insulted unit decides that the nearest friendly unit has insulted it. The insulted unit will immediately charge the other unit (which is allowed to react as usual). If there is no friendly unit within charge reach and the insulted unit is able to shoot it will shoot at the nearest friendly unit (if this unit is in close combat, in case of a successful hit decide which unit is hit by chance). In any other cases the insulted unit will take no action except to react or defend in close-combat. |
| 2-5 | "WHAT did you say?" | The insulted unit is fairly certain it heard an offensive sound from the nearest friendly unit. The insulted unit spends the turn hurling insults at its team member and may do nothing eslse this turn except react or defend in close-combat. |
| 6 | "I'll show you!" | The insulted unit imagines that its team mebers are laughing about it behind its back. To show them up the insulted unit decides that it will be the first one to the scrap: The insulted unit must move as quickly as possible towards the nearest enemy unit, charging into close-combat if possible. If there are no enemy models within sight, the will make a normal move immediately in this situation in addition to the regular movement phase in a way that it has better chances to be in charge range to the next enemy in the movement phase (so it maybe will move twice). |

Designer's note: This is based on mortheim animosity rules for Orks & Goblins (found in Mordheim rule book compendium - the Edinburg revision, assembled by Paul H., pg162).

#### FRENZY

Some units can work themselves into a berserk state of fury, a whirlwind of destruction in which all concern for their own personal safety is ignored in favour of mindless violence. These units are described as being frenzied.

Frenzied models must always charge if there are any enemy models within charge range (check after
charges have been declared). The player has no choice in this matter – the unit will automatically declare a charge.

Frenzied units fight with double their Attacks characteristic in hand-to-hand combat. Warriors with 1 Attack therefore have 2 Attacks, units with 2 Attacks have 4, etc. If a unit is carrying a weapon in each hand, he receives +1 Attack for this as normal. This extra Attack is not doubled.

Once they are within charge range, frenzied units are immune to all other psychology, such as fear and don’t have to take these tests as long as they remain within charge range.

If a frenzied unit becomes shaken, the unit is no longer frenzied and continues to fight as normal for the rest of the battle.

#### HATRED

Hatred is a very powerful emotion, and during this era of strife and war, bitter rivalry is commonplace.

Units fightining enemies they hate in hand-to-hand combat may re-roll any misses when they attack in the first turn of each hand-to-hand combat. This bonus applies only in the first turn of each combat and represents the unit venting his pent-up hatred on his foe. After the initial round of hand-to-hand combat he loses some impetus and subsequently fights as normal for the rest of the combat.

#### STUPIDITY

Units that are stupid test at the start of their turn to see if they overcome their stupidity. Make a test for each model affected by stupidity. If you pass the test by rolling their Leadership value or less on 2d6 then all is well (it is allowed to use tacticts card to pass Ld tests to pass a Stupidity tests) – the creatures behave reasonably intelligently and the player may move and fight with them as normal.

If the test is failed all is not well. Until the start of his next turn (when it takes a new Stupidity test) the model will not cast spells or fight in hand-to-hand combat (though his opponent will still have to roll to hit him as normal).

If a model who fails a Stupidity test is not in hand-to- hand combat, roll a 1d6:

- 1-3: The unit moves directly forward at half speed in a shambling manner. It will not charge an enemy (stop his movement 1,5" away from any enemy he would have come into contact with). It can fall down from the edge of a sheer drop (see the Falling rules) or hit an obstacle, in which case he stops. The model will not shoot this turn.

- 4-6: The unit stands inactive and drools a bit during this turn. He may do nothing else, as
drooling is so demanding.

Regardless of whether the test is passed or failed, the result applies until the start of the model’s following turn (when it takes a new Stupidity test).

# Processing an ingame day

## Example for a schedule for two ingame days on one real day

- 11:00-11:30: Preparation (30min)
- 11:30-13:30: 1st Mission (2h)
- 13:30-14:15: Postprocessung (45min)
- 14:15-15:00: 1st Arena (45min)
- 15:00-15:30: Kill Tea Time (30min)
- 15:30-17:30: 2nd Mission (2h)
- 17:30-18:15: Postprocessing (45min)
- 18:15-19:00: 2nd Arena (45min)

Designer's note: In the beginning of the campaign you shouldn't expect to manage processing 2 ingame days but just 1 because there will be a lot of special rules to consider. The much more chilling way is to play just 1 ingame day per day to have for talking and discussing.

## Match the Kill Teams to maps and portals

Kill Teams never know which other Kill Team they will meet in the Fortress.

When all participants are ready to start the exploration 1 randomly chosen player riffles the matching cards places 2 on each prepared Blackstone Fortress battlefield with front side down, drawing one card from top and one card from the bottom and placing them on the portals which have the most distance to each other.
If the amount of participants are off-numbered one battlefield will be for 3 Kill Teams.

The Intention is that a battle starts just by deciding *who* places units first, not *where* (because you never know where the Blackstone Fortress bring you).

## Exploration missions in the Blackstone Fortress

### Define the middle of the map

Before starting a game define the middle hex field of the map. It doesn't need to be the exact middle but it should be the hex field that is most in the middle. Define which side of the 6 sides of the hex field is side 1 (e.g.: maybe by placing an eye-catching 1d6 at this side showing "1" on the top).

### Get a mission

One of the matched players chooses one of the missions described in the rules for Kill Team on Blackstone Fortress maps (White Dwarf June 2019) by rolling 1d6:

#### 1-3: Territorial Imperative

- **Scouting Phase**: There is no Scouting Phase.

- **Deployment**: The players roll 2d6. The winner can decide who deploys first (the start portal of each team was already defined according to the rules in section "Match Kill Team and portal by chance"). The players deploy their units one by one alternating. Players without units left are skipped.

All units have to be deployed *completely* within a 3" radius around the start portal, measured from the edge of the portal.

If there are units left not fitting into the radius they have to be beployed in a battle round 2 starting with one edge of the base in contact with a portal field edge or point from where they start to move.
In the begin of battle round 3 the portal will close and missing units will return to Precipice and they will be counted as not have participated related to experience.

- **Battle length**: The battle ends with round 4.

- **Victory conditions**: The Kill Team with the highest count of victory points win. If all Kill Teams have the same count of victory points the match is a draw. Kill Teams will receive Victory Points during the mission in the following way:

- +2 for any controlled objective marker
- +1 if the opponent Kill Team(s) is/are broken
- +2 for any killed leader
- +1 for any killed specialist

#### 4-6: Hidden Vault Datacube (HVD) Hunt

- **Scouting Phase**: Same as in "Terrorial Imperative".

- **Deployment**: Same as in "Terrorial Imperative".

- **Battle length**: Same as in "Terrorial Imperative".

- The "Hidden Vault Datacube" (HVD): At the end of each battle round all player starting with the one who has the initiative choose a single objective marker which is within 3" around own units and roll 1d6. On 5+ the HVD was found. Remove all other objective markers.
There can be only one roll for each objective marker per battle round except the HVD was not found at the end of battle round 4 - in this case go on rolling 1d6 player by player until the HVD was found.

- **Victory conditions**: A Kill Team will win the mission under the following circumstances:

- if it controls the HVD marker at the end of the last battle round
- if it is the last Kill Team on the field.

Designer's note: The section "Get a mission" is inspired by the rules for Kill Team on Blackstone Fortress maps presented White Dwarf June 2019.

#### The Hidden Vault

If you have 4 HVDs (however you got it) your Kill Team has full control over the "Maglev transporter" (which normally transports you Kill Team to a random part of the Blackstone Fortress enetering it via a portal). With control over the Maglev transporter you can choose the Hidden Vault as target (or for any other Kill Team - that's your choice, of course).

- **Scouting Phase**: Same as in "Terrorial Imperative".

- **Deployment**: Same as in "Terrorial Imperative".

The Hidden Vault is protected by the Blackstone Queen, flanked by 2 Blackstone Guardians:

Blackstone Queen

NAME M WS BS S T W A Ld Sv Max
Queen 6" 2+ 2+ 5 5 6 5 10 2+ 1

This model is armed with a Blackstone spear.

This weapon can be used as a ranged weapon and a melee weapon. When making shooting attacks or firing Overwatch with this weapon, use the ranged profile; when making close combat attacks, use the melee profile.:

Ranged 24" Rapid Fire 1 4 -1 2 -
Melee Melee Melee +1 -3 D3 -

Abilities:

Aegis of the Fortress: The unit will never become shaken. In addition, roll a 1d6 each time a model with this ability suffers a mortal wound in the Psychic phase. On a 6 that mortal wound is ignored.

Slayer of Intrudors: When this model piles in and consolidates, they can move up to 3" towards the nearest enemy Leader or COMMANDER even if it is not the nearest enemy model, so long as they finish this move within 1" of an enemy model.

Storm Wings: The unit has a 3+ invulnerable save.

Superior Fortress Physiology: Ignore the penalty to this model’s hit rolls from flesh wounds.

Specialists: -
Faction Keywords: BLACKSTONE CONSTRUCT, UNALIGNED, FLY
Keywords: INFRANTRY, PROTECTORS OF THE BLACKSTONE FORTRESS

Blackstone Guardian

NAME M WS BS S T W A Ld Sv Max
Guard 6" 2+ 2+ 5 5 4 4 10 2+ 2

This model is armed with a Blackstone spear.

This weapon can be used as a ranged weapon and a melee weapon. When making shooting attacks or firing Overwatch with this weapon, use the ranged profile; when making close combat attacks, use the melee profile.:

Ranged 24" Rapid Fire 1 4 -1 2 -
Melee Melee Melee +1 -3 D3 -

Abilities:

Aegis of the Fortress: The unit will never become shaken. In addition, roll a 1d6 each time a model with this ability suffers a mortal wound in the Psychic phase. On a 6 that mortal wound is ignored.

Slayer of Intrudors: When this model piles in and consolidates, they can move up to 3" towards the nearest enemy Leader or COMMANDER even if it is not the nearest enemy model, so long as they finish this move within 1" of an enemy model.

Storm Wings: The unit has a 3+ invulnerable save.

Superior Fortress Physiology: Ignore the penalty to this model’s hit rolls from flesh wounds.

Specialists: -

Faction Keywords: BLACKSTONE CONSTRUCT, UNALIGNED, FLY

Keywords: INFRANTRY, PROTECTORS OF THE BLACKSTONE FORTRESS

- **Battle length**: Same as in "Terrorial Imperative".

- **Victory conditions**: A Kill Team will win the mission under the following circumstances:

- if it controls the chamber at the end of battle round 4.
- if it is the last Kill Team on the field and all PROTECTORS OF THE BLACKSTONE FORTRESS are out of action.

If your Kill Team won the mission your are the winner of the campaign.

Designer's note: The units are based on Adeptus Custodes profiles of elite rules pg54.

### Get a subplot

On a mision inside the Blackstone Fortress strange things can happen. Each player on a mission rolls 2d6 to choose a subplot from the following list:

- 2: **"Maglev transporter doors closed too early**: The player who rolled this result must remove a randomly selected unit, other than their Kill Team leader (or a COMMANDER, if available), from their Kill Team for the mission - the unit didn't manage to enter the Maglev transporter in time and went back to Precipice.

- 3: **Toxic dust**: Any units who are down at the end of the mission go out of action on a roll of 1-3 rather 1-3. If both palyers roll the result, then all units who are down at the end of the mission go out of action.

- 4: **Lightningh Strike**: The player who rolled this result can make a strategical wiuthdrawal at the start of any battle tound if the player's Kill Team has taken an enemy unit out of action. The player can do this even if their Kill Team has not suffered casualties.

- 5: **Corrosive Slick**: Roll 1d6 for any unit who advances or charges during the mission. On a roll of 1, that unit gets a mortal wound at the end of their move as they slip in the treacherous slick. If both players roll this result, the units get a mortal wound on a roll of 1 or 2.

- 6: **Hidden Cache**: The player who wins the mission gets +1 ArcheoTech to spend on their armament phase. If both players roll this result, the player who wins the mission gets additional +1 ArcheoTech.

- 7: **"It's Quiet, too quiet."**: Your trigger for your Random Encounters is 4+ (so a chance of 1/2 instead of 1/3).

- 8: **Vital Mission**: The player who wins the mission can re-roll the dice to determine how many ArcheoTech was found. If both palyers roll this result, the player who wins the mission will receive 1 additional ArcheoTech.

- 9: **Indomitable**: The player who rolls this result can subtract 1 from any Ld tests during the mission (including the check if a Kill Team is broken).

- 10: **Fear and Confusion**: Add 1 to all Leaderhip tests in the mission for both players. If both players roll this result, add 2 to all Leaderup tests inseatd.

- 11: **Bitter Rivalry**: The player who rolled this result randomly selects a unit from the Kill Team and a unit from their opponent's Kill Team. These units hate each other. Make a not of this on the according CMCs - this rivalry is eternal.

- 12: **Friendly Territory**: The player who rolled this result can re-roll Serious Injury rolls of 11-21 for they Kill Team Members.

Designer's note: The section "Get a sublopt" is inspired by "Shadow War Armageddon" (2017) rule book p99.

### Play the mission

Play the mission according to the rules for KT on BSF maps. Summary:

- **White lines**: White lines repesent partial cover. Handle white lines as obstacles which are 1" high and deep so that you can leap over these obstacles without any penalties.

- **Red lines**: Red lines are like walls in Arena - you can not move through red lines in any way and you have to measure all distances around red lines (not through them, even not for "aura" abilities).

#### ...with Random Encounters (the Fortress is alive)

In the beginning of each battle round each player has to roll 1d6. A result of 1 and 2 indicates a random encounter. The player has to roll a d66 to see what exactly happens (see "d66 results"). 

Some random encounters are about that something appears in a randomly chosen part of the battlefield or in a random portal or affects a random unit. The portals of your map should be already marked if you follow the rules so just roll 1d6 to choose it (re-roll if the result would point on a non-existing portal). For the other cases see "How to choose a random hex field" and "How to choose a random unit".

Designer's note: This section is based on the Mordheim rules for random encounters by Mark Havener and Tim Huckelbery (no idea when or where presented for the first time).

- **How to choose a random hex field:**

Roll 1d6. According to the outcome count the hex field sides starting with side 1. Roll another 1d6 and count away from the middle hex field according to the outcome of the 2nd 1d6 - this is the random hex field where the random encounter happens!
If this field is outside of the map choose the field which is still a part of the map.
If a unit shall appear in a random hex field it appears in the middle of the field but never directly in close combat with a unit (if this would be case place the model so that there is 1,5" distance between the base edges).

- **How to choose a random unit:**

If one of your units is affected randomly mix your CMCs and let your opponet draw one CMC with closed eyes - this is the affected unit.

- **How to handle appearing units:**

Treat new units on the battlefield which does not became part of your Kill Team (yes, this can happen) as own, separated Kill Teams (without a Leader and no tactics but with own initiative) which never become broken.

Rolls for these units are always rolled by the member which is not affected. If both are affected try to find a compromise, maybe by rolling 2d6 rolls together (each 1d6 adding the results).

##### 11: Unit event: "Shrike is here!"

The Flayed One called "Shrike" appears in a randomly chosen hex field.

Called "Shrike" by the few individuals surviving an encounter with him he is a Necrontyr who not lost his original mind completely during the period of biotransferance which replaced the immortal bodies of the Necrontyr with living metal during the fatal alliance with the C'tan.
Shrikes existence is determined by the tragody that he "lost" his wife, his dearly beloved "Samaneh", during this process because she became one of the soulless machines when the race of Necrontyr was transformed into what we know as the Necrons today. Since then Shrike became more insane with any day living in the universe for millions of years without his great love. While his wife is laying in a stasis-crypt Shrike is trying to find a way transforming her mind back into the original one hoping, the secret of the Blackstone Fortress will offer what he is looking for.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Shrike	5"	3+	6+	4	4	3	4	10	4+	1
TODO: add table
This model is armed with flayer claws (Range: Melee, Type: Melee, S: User, AP: 0, D: 1, Abilities: Re-roll failed wound rolls for this weapon).

ABILITIES:

- Reanimation Protocols: When an Injury roll is made for this model, on an unmodified roll of 6 the model is not taken out of action and does not suffer a flesh wound. Instead it is restored to 1 wound remaining with no flesh wounds.

- Translocation Beams: If Shrike Advances, you can re-roll the 1d6 to determine the increase to that model’s Movecharacteristic. In addition, if Shrike Advances, it can move across models and terrain as if they were not there.

- Torned existence: If one or more Necron Kill Team participate roll 1d6 at the beginning of any battle round. On 1-3 Shrike will attack the Necron Kill Team(s) - Shrike is half Necron, half Necrontyr hating what his race became which furthermore took away his wife. On 4-6 Shrike will attack any other participating Kill Team for the current battle round because the Necron inside him controls his actions.
If all participating Kill Teams are Necrons Shrike will join the Team with the lowest Mission Glory in the first round and stays a member of this Kill Team for the current mission but will attack his Kill Team on 4+ in the following battle rounds because of his torned existence.

- Since Shrike is fighting since aeons so he is Combat Specialist Level 4 with the following abilities: EXPERT FIGHTER (+1 attack), WARRIOR ADEPT (+1 to hit rolls), DEATHBLOW (wound roles of 6 inflict 1 mortal wound on the target in addition to any other damage) and BLOODLUST (re-roll failed charge rolls).

- Shrike causes FEAR.

- Shrike is FRENZY.

KEYWORDS: INFANTRY, FLAYED ONE

All units wounding Shrike successfully will receive +1 EXP after the battle and the unit killing Shrike gets an additional +1 EXP. If Shrike is killed, place an objective marker at his last position. The Kill Team controlling this objective marker at the end of the game will find +1 additional ArcheoTech. If this ArcheoTech is analyzed this will be successful and will provide the "Translocation Beams" ability of Shrike if worn by a unit.

Designer's note: This event is an replacement for "Collapsing Building" of the org. Mordheim Random Encounters list (case 31) - there are no buildings inside the Blackstone Fortress. Because there was no special even for Necrons this event was created to bring in some spirit of the oldest race in the Warhammer 40k universe: "Shrike" is a Flayed One adaption inspired by [Mortal Engines - Shrike Featurette (HD)](https://invidio.us/watch?v=nbtT-ZXXhQ4) - so maybe the other way around of the way Peter Jackson took creating "Shrike" by looking at GW's Necrons?

##### 12: Unit event: Swarm of rats

Your Kill Team has frightened the rats that live in the Blackstone Fortress. The rats appear in a random hex field and are in a hurry to get away and will attack anything that stands in their way. Use a template 80mm x 120mm to represent the rats. Six monster bases (use rat swarms if you have them), placed in a rectangle with one short side representing the front, will do nicely. The rats move 2d6" each turn coming from your start portal. They will always move forward in a direct way to one of the other portals (roll a dice to choose which one). Come to an intersection roll randomly to determine which direction they take (example: for a normal four-way cross-intersection roll a D6: 1-2 right, 3-4 straight forward, 5-6 left). The rats cannot be engaged in close combat and if they make contact with a model they will run right over him (models with the keyword FLY are only affected by the rats if they are already in close combat with at least one unit which can not FLY as well). Any model moved over by the rats will suffer D3 Strength 2 hits, normal armour saves apply.

##### 13: Fortressquake

Powerful energies from the Fortress shake the local landscape. The ground heaves and buckles violently and Kill Team units find it hard to keep their footing.

Roll a 1d3 to determine how many game turns the earthquake lasts (place a die as countdown for the remaining battle rounds)).

While the shaking persists, for units which can not FLY

- all movement is halved
- all Shooting and Close Combat to hit rolls are made with a -1 penalty.

##### 14: Cave storm

A powerful wind screams through the area, wildly scattering debris and knocking down anything that is not firmly anchored to the ground. Kill Team units now suffer a -1 penalty for advancing and charging and to hit rolls (close combat and shooting) for the rest of the game.

##### 15: Unit event: "Blood for the Blood God!"

Unfortunately for the Kill Team involved, the scent of blood of the ongoing battle has brought the attention of one of Khorne’s minions. Reality is breached as a vicious Bloodchaser emerges from the Realm of Chaos in a random portal to shed even more blood for its master. The Bloodchaser has the following characteristics and special rules:

Profile		M	WS	BS	S	T	W	A	Ld	Sv
Bloodchaser	6	3+	3+	4	3	1	2+	10	4+

The unit is amred with an Abyssblade (Range: Melee, Type: Melee, S: User, AP: 0, D: 1, Abilities: automatically causes a critical hit if a 4+ is rolled to hit which mean's no armour but only invulnerable saves are allowed).

- Fear: As monstrous and horrifying creatures, Bloodchasers cause fear.

- The Bloodchaser is immune to all psychology tests and passes any Leadership based tests it is required to make.

- Bloody gift: If the Spawn is encountered by a KHORNE Kill Team it will join this team for the current battle. If there are 2 or more KHORNE Kill Teams participating the Bloodchaser will join as described in section "Choosing a side for possible 

The Bloodchaser will seek out the nearest close combat and join in, drawn by the clash of steel and has a number of Attacks equal to the number of opponents it is fighting (down to a minimum of 2 Attacks). It will split its attacks amongst the opponents, and no matter how many units are involved it may roll to hit each one at least once. It will also prevent an opponent from taking any other unit out of action in the massed combat, as they will be too concerned with the Daemon to finish off their other enemy!
If there are no ongoing close combats within range, it will charge the model with the highest Weapon Skill in order to do battle with a worthy opponent. If there are no enemies within charge range, the Daemon will run towards the nearest unit, eager to do battle.

##### 16: Lucky Find

One random unit not in hand-to-hand combat or shaken has stumbled upon a lair of ArcheoTech! Assuming he doesn’t go out of action, +1d3 ArcheoTech is added to any other ArcheoTech the Kill Team finds after the match. If he is taken out of action in close combat, the enemy model steals the shard away!

##### 21: Unit event: Restless Spirit

Countless unfortunates have suffered agonising deaths in countless forms in the Blackstone Fortress. Not all of these individuals can easily accept their new condition and they refuse to rest. Perhaps they left some important task unfinished or seek revenge on those who did them harm. The Kill Team has stumbled upon one such spirit which appears in a random hex field.

Any Kill Team unit who is within 8" of the spirit at the start of its Movement phase must make a Ld test or flee with an advancing move. This creature is unable to affect (or be affected by) the physical world, but it is very frightening nonetheless. The spirit moves 4" in a random direction, moving through walls (even walls with red lines), obstacles or Kill Team units as if they were not there.

The spirit will not charge and cannot be harmed by the Kill Team members in any way. The only exception to this is if the spirit comes into contact with a Grey Knight unit (either indirectly or directly by any kind of movement of the Grey Knight unit). A Grey Knight may choose to put the dead to rest. If the player controlling such a model decides to do this, the spirit is immediately banished (disappears and does not return) and the Grey Knight unit gains +1 EXP.

If not banished the spirit stays for the whole battle if it not accidently moves through a portal field out of the battle field.

##### 22: Burning field

Suddenly, a location of one of your units bursts into flames. Any models inside the same hex field take a S3 hit unless they move outside in that turn and any models within 2" of the field take a S2 hit from the smoke and heat unless they move further away as well. For the rest of the game, the hex field itself will cause fear due to the intense flames and anyone who wishes to enter it must first pass an Ld test exactly as if they wished to charge a fear-causing enemy.

##### 23: Unit event: Brickgrappletree

A randomly determine unit of the encountering player’s Kill Team finds suddenly notices that the stone formation behind it is not just one of the weird stone structures which are common in the Fortress but something living, a Brickgrappletree. That model is attacked as a large mouth opens up in the trunk of the Brickgrappletree and its branches whip down to grab its unfortunate victim. Place a Brickgrappletree in the middle of the hex field of the victim. This and any other unit (even partially) in the same hex field of the affected unit is now considered to be in close combat with the Brickgrappletree, which has the following characteristics:

Profile				M	WS	BS	S	T	W	A	Ld
Brickgrappletree	0	3	0	4	6	3	2	10

If there are no units left in the hex field of the Brickgrappletree it will attack any unit in any hex field which is directly connected with the one of the Brickgrappletree.

The Brickgrappletree automatically passes any Leadership tests it is required to make. Any result on the Injury table will be counted as flesh wound so it needs 4 flesh wounds to take it out of action.

Opponents are +1 to hit the tree, due to the fact that it is rooted to the spot!

If the Brickgrappletree takes a Kill Team unit out of action, the unit will be eaten if it has a base size up to 25mm and if not rescued. If no friendly model comes within 1" of where the model fell by the end of that Kill Team’s next turn, the fallen member is considered lost.

##### 24: Unit event: Runaway Ur-Ghuls

1d3 Runaway Ur-Ghuls appear in a random portal moving as quickly as possible towards the nearest Kill Team units and engage them in close combat as soon as they are able.

Profile			M	WS	BS	S	T	W	A	Ld	Sv
Runaway Ur-Ghul	8	3+	-	4	3	3	4	4	7+

The unit is amred with claws and talons (Range: Melee, Type: Melee, S: User, AP: 0, D: 1, Abilities: -).

Abilities:

- Insensible To Pain: This model has a 5+ invulnerable save.

- Ferocious Charge: Add 2 to this models' Attacks characteristic if it made a charge move in the same turn.

- Old masters: At the beginning of the movement phase Drukhari units can try to tame a Runaway Ur-Ghul if a Drukhari unit is within 2" of a Runaway Ur-Ghul with 7+ on 2d6. If successfull the Runaway Ur-Ghul is controlled by the Drukhari player as long a Drukhari unit is within 2" of the Runaway Ur-Ghul. If unsuccessull the Runaway Ur-Ghul will attack the Drukhari unit which tryed to tame it.

Designer's note: BSF40k rules pg16.

##### 25: The Twisting of the Air

Reality itself seems to twist, warping perceptions until no one can be sure of what their senses tell them. Roll 1d6 at the start of each battle round.

For the next 1d3 game turns, the distance within which units can use their special abibilities is the value rolled instead of the usual distance.

All distances between models are increased by the same 1d6 roll for purposes of weapon ranges and charging.

##### 26: Unit event: Spawn

A mindless spawn appears in a randomly chosen portal. Spawn have the following characteristics:

Profile	M	WS	BS	S	T	W	A	Ld	Sv
Spawn	2d6	3	0	4	4	2	2d6	10	

- Faction Keyword: Chaos Daemons

- Fear: Spawn are disgusting and revolting blasphemies against nature and cause fear.

- Psychology: They are mindless creatures, knowing no fear of pain or death. A Spawn automatically pass any Leadership based test it is required to make.

- Movement: The Spawn moves 2d6" towards the nearest model in each of its Movement phases. It does not charge as usual, instead if its movement takes it within 1" of a model it counts as charging and engages that model in close combat.

- Attacks: Roll at the beginning of each Close Combat phase to determine the spawn’s number of Attacks for that phase.

- Daemonic gift: If the Spawn is encountered by a CHAOS DAEMONS Kill Team it will join this Team for the current battle.

##### 31: Ogryn or Bullgryn mercenary

TODO: Tabellen sauber formatieren

An Ogryn or a Bullgryn mercenary appears in a random portal.

Roll 1d6 :

- 1-3: An Ogryn Mercenary appears (elite rules pg62, Bone'ead, Veteran, standard wargear).
- 4-6: A Bullgryn Mercenary appears (elite rules pg62, Bone'ead, Veteran, standard wargear).

The mercenary unit offer its services to the Kill Team with the lowest Mission Glory. If the Mission Glory is the same level it will offer the services to the lowest Victory Points. If the Victory Points are the same it will offer the services to the Kill Team with most units lost in the current battle. If the situation is still a tie roll 2d6, the winner will be chosen by the mercenary.
The mercenary will fight as member of the Kill Team until the end of the current mission. Returning to Precipice the mercenary demands payment for services rendered by 1 ArcheoTech and another 1 if the mercenary fought for the winning Kill Team.

| Profile | M | WS | BS | S | T | W | A | Ld | Sv | Max
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Ogryn Bone'ead | 6 | 3+ | 4+ | 5 | 5 | 3 | 4 | 8 | 5+ | - |

The model is armed with:
- ripper gun (can be used as ranged weapon (Range 12", Type: Assault 3, S: 5, AP: 0, D: 1)
- melee weapon (Range: Melee, Type: Melee, S: User, AP: -1, D: 1)
- frag bombs (Range: 6", Tape: Grenade 1d6, S: 4, AP: 0, D: 1).

Specialism: Veteran

Profile				M	WS	BS	S	T	W	A	Ld	Sv	Max
Bullgryn Bone'ead	6	3+	4+	5	5	3	4	8	4+	-

The model is armed with a grenadier gauntlet (Range: 12", Type: Assault 1d6, S: 4, AP: 0, D: 1), frag bombs (Range: 6", Type: Grenade d6, S: 4, AP: 0, D: 1) and a slabshield (add 2 to saving throws made against attacks that target a model with a slabshield).

Specialism: Veteran.

If the Kill Team cannot (or will not) pay him the mercenary will take out his frustration on 1d3 units of the Kill Team which have survived the day at the end of the day after the Arena fights. The chosen units have to fight against the mercenary one by one in the Arena in same order the units were chosen (representing that the mercenary lies in wait for the cheating Kill Team in the corridors of Precipice). The mercenary is played by the player which was the opponent in the last battle. If the mercenary got flesh wounds or was taken out of action during the fight check the physical constitution. If the mercenary can not go on fighting against the next unit because of certain serious injuries (like "died" or "captured"): congratulations, the unit defeating the mercenray gets +1 EXP.

##### 32: Scrawlings on the floor

A randomly determined Kill Team unit sees writing suddenly appear in blood on the floor under it's feet. Roll on the following table to find out what the writing says:

| 1d6 | Result |
| --- | --- |
|  1  | The writings show a map of the local area. The unit’s Kill Team can modify the roll by +/-1 picking the scenario for the next game they take part in. |
|  2  | Reading the writing accidentally sets off a spell on the reader. The model suffers a minor curse and now has a -1 penalty to all dice rolls for the rest of the game. |
|  3  | The Kill Team unit learns of the remains of some booty inside the building. If he moves inside he finds a 1d6 BlackCoins. |
|  4  | The writings reveal all the hiding places in the area. The model can take advantage of partial covering, even in the open, for rest of the game. |
|  5  | The Kill Team unit learns of a secret passage through the area – if the unit moves it can pop out next turn in any other hex field. |
|  6  | "MENE, MENE, TEKEL, UPHARSIN" - the unit gets an omen and can prevent one single futural successfull wound that is not prevented by a save roll during the current battle. |

##### 33: Thig Fog

A fog rolls in, thick as pea soup. Models can only see 2d6" (both players roll 1d6 and count the results for the distance all models can see; do not roll individually for each model). Re-roll at start of each battle round to see what the visibility is for that turn. 

The fog lasts for the rest of the game.

##### 34: Hands of Blackstone

Hands of stone suddenly jut out of the ground in a hex field of one of your units. Any model partially in this hex field is affected. The area is now very difficult ground as the hands grasp and attempt to hold anyone passing near them. Units which can FLY are affected on 4+ on 1d6. This encounter lasts one game turn, at the end of which the hands sink back into the ground.

##### 35: Unit event: Squad of Spindle Drones

Maybe one of your members touched something within the Fortress that should not have been touched: The fathomless spirit of the Blackstone Fortress sends it's guards to defend the intruders. A squad of 4 Spindle Drones appear in a randomly chosen portal:

Profile	M	WS	BS	S	T	W	A	Ld	Sv
Ambull	6	3+	3+	4	4	2	2	10	4+

This model is armed with a drone pulse (Range: 18", Type: Pistol 1, S: 3, AP: 0, D: 1, Abilities: -).

- Threat Level Rising: Improve the Strength, Armour Penetration and Damage characteristics of a model's drone pulse by 1 for each model in its unit that has either lost wounds or been destroyed. For example, if 2 models in a unit of Spindle Drones have beend destroyed and another has lost 1 wound, the 1 remaining model's drone pulse would have a Strength characteristic of 6, an AP cahracteristic of -3 and a Damage characteristic of 4.

The Spindle Drones move as quickly as they can toward the nearest models, dividing themselves as evenly as they can between multiple targets if they are available.

##### 36: Possessed!

One randomly selected unit of the Encountering Player’s Kill Team is suddenly possessed by a minor spirit for 1d3 rounds (place a die as countdown marker next to the unit). This spirit is far too weak to control the model’s entire body, but is instead limited to controlling one of his or her limbs (usually an arm). The model takes
an automatic hit at his or her own Strength during each of his or her Close Combat phases until the possession is over and may do nothing else. If such a Kill Team unit is the object of the spirit’s attention, randomly determine another Kill Team member to suffer the effect instead.

##### 41: Fountain of Acid Blood

The Fortress itself seems to weep as even more blood is shed on its floor. For the rest of the game, whenever a model looses a wound or is taken out of action, the model that did the deed must take a Strength test or will suffer a hit wounding on 4+ hit by a torrent of acid blood pouring from the ground.

##### 42-44: The Fortress is alive

Cloud like structures rapidly gather above the battlefield in an unnatural, sickly yellow-green mass and warp lightning begins to dance from one cloud to another. Thunder rocks the air, screaming out in almost intelligible noises. The clouds themselves seem to take on the shapes of monstrous creatures and all Kill Teams in the conflict look at each other with fear on their faces: The Fortress is alive!

Roll a 1d6 to see what the manifestation will bring:

1d6	Result

- 1: Warp lightning begins to strike the ground in search of a victim. It will hit the unit with the best armour save, attracted to the large amount of metal. That unit takes a S5 hit, with no armour save possible. If multiple units all have the highest armour save, randomly determine which is struck. The warp lightning will strike for D3 turns before moving on towards another part of the city.

- 2: A blizzard suddenly befalls the Kill Teams. All movement is halved for one turn, but there is no other effect.

- 3: A sudden acid rain burns flesh and eats into stone and metal. Each Kill Team unit takes a single S2 hit (normal save rolls apply) every turn the rain lasts. The rain lasts 1d3 turns.

- 4: A magical mass of lightning forms near the ground, illuminating the area with an eerie greenish glow. It begins to move about the area, drawn to PSYKER powers from which it feeds. Place an objective marker in a randomly chosen hex field to represent the mass and each turn move it 2d6" towards a PSYKER unit with the highest point value. If there are no PSYKER units on the battlefield, the mass will move in a random direction, doing nothing but blocking line of sight as it moves. If the unit the mass is moving towards manifests psychic power, immediately move the counter another 1d6" towards the unit. If the ball of lightning touches the target unit, the unit is frozen in time and cannot do anything. While frozen, the unit cannot be attacked or harmed in any way – the lightning mass protects its prey! After freezing a unit, the lightning will no longer move but will instead remain near its victim to feed. The lightning will feed for 1d3 battle tounds on the PSYKER energy of its victim and then jumps back into the warp, freeing the PSYKER unit. If the game ends before the feeding is done, the victim is immediately released. Victims suffer no long-term ill effects from their exposure to the lightning but will gain +1 EXP if survived for this extraordinary experience.

- 5: A rumbling is heard from overhead, as thunder erupts from the strange clouds. The thunderclaps become more intense and the heavy pulses of air brings units to their knees as if they had been hit by cannonballs of solid air. 1d6 randomly selected models of all Kill Teams will get a hit that wounds on 4+ (save rolls allowed . If any of these models are in close combat, all other members of that melee are affected as well.

- 6: Tendrils of smoke drift down from the clouds, winding around the heads of members of each Kill Team. Randomly select one Specialist from each Kill Team – these units have been chosen by the Fortress. The chosen models must attack each other every turn, will automatically pass all Leadership tests and will fight until only one remains (the combat will end when one of the models puts his rival out of action). If a Kill Team does not have any Specialists left a random unit will be chosen from that Kill Team instead.

##### 45: Unit event: Explorer

A Fortress Explorer appears in a randomly chosen portal. Roll 1d10 to determine which Explorer will appear:

TODO: add unit profiles

###### 01: Adeptus Astartes

| 1d6 | Unit |
| --- | --- |
| 1-3 | [JANUS DRAIK](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf) |
| 4-6 |	NEYAM SHAI MURAD (White Dwarf Sep 2019) |

Profile		M	WS	BS	S	T	W	A	Ld	Sv	Max
Janus Draik	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Neyam 	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

###### 02: Astra Militarum

1d6 Unit
1: 	[TADDEUS THE PURIFIER](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
2:	[PIOUS VORNE](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
3:	[REIN AND RAUS](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
4:	[ESPERN LOCARNO](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
5:	ARADIA MADELLAN (White Dwarf Sep 2019)
6:	GOTFRET DE MONTBARD (White Dwarf Sep 2019)

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Taddeus	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Taddeus ?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Pious	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile		M	WS	BS	S	T	W	A	Ld	Sv	Max
Rein&Raus	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Espern	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Aradia	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Gotfret	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

###### 03: Adeptus Mechanicus

1d6		Unit
1-2: 	[UR-025](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
3-4:	X-101 (White Dwarf Sep 2019)
5-6:	DAEDALOSUS (White Dwarf Sep 2019)

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
UR-025	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
X-101	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile		M	WS	BS	S	T	W	A	Ld	Sv	Max
Daedalosus	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

###### 04: Kroot, T'au Empire: DAHYAK GREKH

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Dahyak	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Designer's note:

- https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf

###### 05: Asuryani: AMALLYN SHADOWGUIDE

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Amallyn	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Designer's note:

- https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf

###### 06: Servants of the Abyss

1d6	Unit
1	[OBSIDIUS MALLEX](https://www.warhammer-community.com/wp-content/uploads/2018/12/ENG_Blackstone_Fortress_Kill_Team_datasheets_v2.pdf)
1-5	CULTIST FIREBRAND (White Dwarf Sep 2019)

Profile		M	WS	BS	S	T	W	A	Ld	Sv	Max
Obsidius	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Cultist	?	?+	?+	?	?	?	?	?+	?

This model is armed with ... (Range: ?", Type: ? ?, S: ?, AP: ?, D: ?, Abilities: ?).

- AbilityX: ?.

- AbilityY: ?.

If the result points on an Explorer which is already part of any Kill Team participating in the campaign roll again (for obvious reasons an Explorer unit can not be there two times - even not in a Blackstone Fortress).

If the Faction Keyword of the appeared Explorer is the same as one of of the participating Kill Teams the Explorer will join this Kill Team. If two or more participants share this Keyword the one with the lowest Glory Points will be joined.

If the Faction Keyword is not represented by any participating Kill Team the Explorer will join a Team according to the following cases:

- If there is a HERETIC ASTARTES Kill Team participating and OBSIDIUS MALLEX appears he will join this team. If two or more participants are HERETIC ASTARTES the one with the lowest Mission Glory will be joined.

- If the Explorer is neither OBSIDIUS MALLEX, DAHYAK GREKH nor AMALLYN SHADOWGUIDE and the encountering Kill Team is an ELUCIDIAN STARSTRIDERS Kill Team the Explorer will join this team.

- If the Explorer is neither OBSIDIUS MALLEX, DAHYAK GREKH nor AMALLYN SHADOWGUIDE and the encountering Kill Team is neither ELUCIDIAN STARSTRIDERS, DRUKHARI, HERETIC ASTARTES, NECRONS, TYRANIDS nor GENESTEALER CULTS the Explorer unit offer its services to the Kill Team with the lowest Mission Glory. 

If the Mission Glory is the same level the Explorer will offer the services to the Kill Team with the lowest Victory Points.
If the Victory Points are the same it will offer the services to the Kill Team with most units lost in the current battle.
If the situation is still a tie gamble rolling 2d6, the winner will be chosen by the Explorer unit.

Example: In a battle between Death Guard, Harlequins and T'au Empire REIN AND RAUS appear. REIN AND RAUS have the Faction Keywords ASTRA MILITARUM which are neither shared by Death Guard nor by Harlequins nor by T'au Empire. Because Death Guard is HERETIC ASTARTES REIN AND RAUS will not join them. Because The Harlequins and the T'au Kill Team have the same Mission Glory, both have 2 Victory Points and both lost 2 units in the current battle the players of both Kill Teams have to roll 2d6 to determine which Team will be supported by REIN AND RAUS.

The Explorer will fight as member of the chosen Kill Team to support the Kill Team's special mission to prevent that shared enemy factions will win the match ("The enemy of my enemy is my friend").

If the joined Kill Team will win the battle the Kill Team will pay the Explorer 1 Archeotech.

###### 46: Plague of insects

An enormous cloud of insects appears in the sky and swoops down on the Kill Teams below. All models have an extra -1 to hit penalty when shooting or fighting in close combat as insects buzz around them and into open orifices. The insects remain for 1d3 battle rounds and then fly away.

##### 51: Sinkhole

An enormous mouth suddenly opens up under the feet of one randomly determined Kill Team unit and all other units (partially) sharing the same hex field. Make an Streangth test for the model. If the test is failed, the model falls into the pit. If a 1 is rolled for the test, the model is sucked underground and taken out of action. Any other failure results in the unit catching himself in the opening, which closes around him. The unit may not move at all but is counted as in partial cover.

##### 52: Horror in the Fortress

Randomly select one unit which is not within 3" of another friendly unit (if this is not the case for any unit, ignore this result and re-roll on the Random Encounters table). 

Unfortunately for the unit it is unit not as alone as it thought. The unit hears a slight ripping sound as if the air itself is being torn apart and something manifests itself behind it. Playe an objective marker in the middle of the hex field of the unit. 

The unit must make a Fear test and if it fails, in the next Movement phase the unit will run screaming 2d6" in the direction where the most of its Kill Team members are and can do nothing else in that turn.

If he passes the test, the unnatural presence still forces the unit 1d6" in the direction where the most of its Kill Team members are, but the unit suffers no other effects.

At the start of his subsequent turn, a fleeing model can try to recover from his ordeal by passing a Leadership test, but at a -1 penalty. If successful, it stops, but cannot do anything else that turn other than catch his breath. If he fails, it again moves 2d6" but in a random direction this tim, trying in vain to erase the terrifying images from his memory.

For the rest of the game, the hex field itself will cause fear, and anyone who wishes to enter it must first pass a Fear test exactly as if they wished to charge a fear-causing enemy.

##### 53: Unit event: Plague Victims

Disease is a common occurrence among the few survivors left in the Fortress. This group of a 1d6 adventurers has contracted a particularly nasty plague known as Nurgle’s Rot and appear in a randomly chosen portal.

Based on 4" movement the plague victims will always advance towards the nearest Kill Team units, seeking their help. If they come into contact with a Kill Team unit's base, they will not attack, but will instead cling to him as they beseech his aid, hindering him greatly.

A model with Plague Victims in contact with 

- moves at half rate
- may not charge or advance
- can just shoot with -1 on hit rolls

If one or more of the Plague Victims is (successfully or not) charged, shot at, or otherwise attacked, these pitiful victims will rout immediately.

If a Plague Victim comes into base contact with a Death Guard unit the unit will turn into a Poxwalker and becomes part of the Death Guard Kill Team.

At the end of the game, roll a dice for each Kill Team unit that got in contact with one or more Plague Victims except for Death Guard units and a roll of 1, that member has contracted Nurgle’s Rot. Roll again to see what effect the disease has on that member:

1d6	Result

. 1: Major Symptoms: The plague races through the victim’s system, producing horrible disfiguring lesions before killing him. Roll 1d3 times on the Serious Injury table (ignoring "Robbed", "Bitter Enmity", "Captured" and "Sold to the Pits" results) to determine the long term effects of the disease.

- 2-5: Minor Symptoms: The plague takes its toll on the victim as it takes its course. Without proper bed rest the victim will die. The Kill Team unit must miss the next game as he
recovers.

- 6: Full Recovery! The victim has an especially hardy constitution or gets an extremely mild case of the disease. The victim suffers no ill effects and gets +1 EXP.

##### 54: Unit event: Last Battle Sister Standing

The horrors of the Fortress can drive even the strongest mind past the point of madness. A Sister Repentia, former Leader and last member of her annihilated Kill Team, appears in a random portal. She has seen her entire Kill Team cut down around her and the experience has proven too much for. She now seeks revenge and is not particular about who gets to pay!

If one of the participating Kill Teams is ADEPTA SORORITAS the Sister will join this team even though there is already a Sister Repentia in this team. In this case the player of this team will control the Sister as part of the of the ADEPTA SORORITAS kill team.

The Sister will in any case move as quickly as possible towards the nearest enemy unit and will try to charge it if possible:

| Profile | M | WS | BS | S | T | W | A | Ld | Sv | Max |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Sister Repentia | 6" | 3+ | 3+ | 3 | 3 | 1 | 2 | 8 | 7+ | 1 |

This model is armed with a Penitent-Eviscerator (Range: Close combat, Type: Close combat, S: x2, AP: -3, D: 2, Abilities: If you attack with this weapon you have to substract 1 from hit rolls).

Abilities:

- Shield of Faith: Models with this ability have an invulnerable save of 6+.

IN addition models with this ability can try to ban a PSY force in any PSY phase like any PSY. Trying this use 1d6 instead of 2d6; the PSY force is banned if the die roll is higher than the die dice roll of the roll the PSY force was manifested with.

- Zelot: You can re-roll hit rolls for close combat weapons for models with this ability in any battle round in which the model charged or was charged.

- Consolation by Pain: Any time this model looses a wound roll 1d6; the wound is not lost on 5+.

The Sister is Zealot Lvl4 with the following abilities:

- *Frenzied*: You can add 1 to this model’s Attacks and Strength characteristics in a battle round in which they charged.
- *Exultant*: Opponents must re-roll unmodified hit rolls of 6 for models from their kill team within 3" of this model, as long as it is not shaken.
- *Strength of Spirit*: Subtract 1 for Injury rolls made for this model.
- *Rousing*: Add 1 to the Leadership characteristic of models from your kill team within 6" of this model, as long as it is not shaken.

If the Sister is taken out of action, leave her body where it fell, any Kill Team unit may loot her body by moving into contact with it during their Movement phase. If this unit is later taken out of action as well, place a counter where the unit fell to represent the Sister’s equipment. This can then be picked up by a different model, as above.

Designer's note: Based on annual 2019 pg107.

##### 55: Booby Traps

Some nefarious individuals (or the Fortress?) have trapped the whole area the Kill Teams are searching.

These traps may take the form of spiked pits, mines, etc. When this encounter is rolled, the Encountering Player must randomly determine which of his Kill Team units has discovered the first trap. This unfortunate individual springs a trap immediately and the unit takes a Strength 3 hit; save rolls apply as normal.

From this point until the end of the game, each player will roll a 1d6 at the start of his or her Movement phase. A roll of 1 means that one member of that player’s Kill Team has sprung a trap and the unit takes a S3 hit as above; apply any damage before the model is moved.

##### 56: Blackstone tunnels

Caused by a sudden gravity hole the ground gives way under one randomly selected unit and the unit falls into the depths of some kind of tunnels curling through the Fortress. The unit takes a hit that wounds on 4+ from the fall and lands near the remains of others who have fallen before. Units which can FLY have an (additional) invulnerable save of 5+. HARLEQUINS will be protected by their Flip Belt. Assuming the unit is not taken out of action by the fall, the unit discovers one of the following after a quick search. Roll a 1d6:

1d6	Result
1	2d2 ArcheoTech
2-5	3d10 BlackCoins
6	1 Hidden Vault Datacube

Unless the unit can FLY or is a HARLEQUIN it is stuck in the catacombs and needs the current round to find a way out of the tunnels, appearing inside another randomly chosen hex field.

##### 61: Forbidden Fruit

Ghostly white flowers suddenly open on a stone formation emitting a very powerful fragrance. Randomly determine a unit of the Encountering Player’s Kill Team who happens to be standing on the the flowers when it comes to life. Place an objective marker directly under the unit's base.

Any Kill Team units within 5" of the marker must pass a Leadership test at the start of each battle round or move as quickly as possible towards the tree.

If within 1" of the tree, a unit will pick and eat one of the swollen, blood-red fruit hanging from its branches. Any model eating one of the fruit is automatically taken out of action, incapacitated by powerful poisons.

A non-spellbound Kill Team unit may keep another model from moving toward the
tree by moving into base to base contact and holding the unit back. Neither model may do anything else while the spellbound model attempts to move to the tree and the restraining model attempts to prevent him from doing so.

Both spellbound and restraining models can react normally if attacked in close combat and a restraining model can give up his attempts at any time. This encounter lasts the remainder of the mission.

Close inspection of the flowers reveals the bones of several creatures overshadowed with dust: The Kill Team which has the most non-spellbound units wihtin 1" of the flowers will get +1 additional ArcheoTech.

##### 62: Losts of the Abyss

The Blackstone Fortress has become a home of a faction called "Servants of the Abyss", a chaos faction living in the Fortress seeking for it's control. Some members of this faction lost their mind staying in the Fortress around-the-clock marauding adventurers.

Roll another 1d6:

- 1: 	3...
- 2-3: 	2...
- 4-6: 	1...

...Lost Negavolt Cultist(s) appear in a random portal moving as quickly as possible towards the nearest Kill Team units and engage them in close combat as soon as they are able.

Profile					M	WS	BS	S	T	W	A	Ld	Sv
Lost Negavolt Cultist	6	3+	4+	3	3	1	3	8	6+

This model is armed with electro-goads (Range: Melee, Type: Melee, S: +2, AP: 0, D: 1, Abilities: Each hit roll of 6+ made with this weapon scores 3 hits).

Abilities:

- Fanatical Devotion: Each time this model lsoes a wound, roll 1d6: on a 5+ the model does not lose that wound.

- Voltagheist Field: This model has a 5+ invulnerable save. After this model has charged, choose on of the target models within 1" and roll 1d6. On a 6 that target model suffers 1 mortal wound.

- Old servants: At the beginning of the movement phase Servants of the Abyss units can try to convince a Lost Negavolt Cultist if a Servants of the Abyss unit is within 2" of a Lost Negavolt Cultist with 7+ on 2d6. If successfull the Lost Negavolt Cultist is controlled by the Servants of the Abyss player as long a Servants of the Abyss unit is within 2" of the Lost Negavolt Cultist. If unsuccessull the Lost Negavolt Cultist will attack the Servants of the Abyss unit which tryed to convince it.

##### 63: Reflecting Pool

One unit on the ground (selected at random from the encountering Player’s Kill Team) notices a small pool of what looks to be still water. Reflecting the gloomy sky above, it appears to be liquid metal or unnaturally deep silvery water, rippling only slightly with the dank breeze blowing through the city. He can ignore it, or bend quickly to peer into its depths. If he’s brave enough to gaze into the murky liquid, roll 1d6:

1d6	Result
1	The water reflects back nightmare images of his own demise, filling him with fear for his own safety. For the rest of the game, no matter how far away he is from friendly models, he will always count as being all alone for Ld based tests.

2	The unit glimpses an image of what is yet to come. For the rest of the current turn the unit may re-roll (once!) any shooting or close combat rolls to hit.

3	A faint image of his personal god appears, be it the Omnissiah or a Chaos God. Filled with courage, the unit will pass any Leadership-based test the unit is required to make for the rest of the mission.

4	The unit peers into the depths of his own mind, unlocking untapped abilities. The unit can detect any enemy models covered that turn, even those not in the unit's normal line of sight, and passes the information on to the rest of its Kill Team. All enemy models lose their covered status for the current battle round.

5	A slender arm reaches out from the pool, leaving no ripples in the smooth liquid, and pale fingers touch the unit’s chest. The soft caress causes a faint glow, which spreads throughout his body. Though it quickly dims away, a strong feeling of strength and vitality is left behind. The unit may ignore the next wounding hit received, even if it is a critical hit and will recover all lost wounds and flesh wounds.

6	The Fortress chooses to reveal its true visage to the unit, unveiling the unknown intelligence that lurks behind the facade of simple ruins and rubble. The unit’s mind is overwhelmed by the enormity of the impression and stumbles away in stark terror. For the rest of the game, all enemy models count as causing FEAR to the unit. After the mission the effects will wear off but will maybe come back. In the beginning of any mission roll 1d6 for the unit: On a 1 the unit’s mind is overwhelmed again with the described results. For any time the unit get's this encounter another time during another mission the roll gets a malus of -1.

##### 64: Screaming Walls

Faces appear in a randomly chosen hex field start to emit a piercing shriek. Any Kill Team units within 8" of the middle of the hex field take a S1 hit (no armour saves allowed) and are at -1 on all hit rolls (close combat and shooting) while the screaming lasts.
PSYKERS are even more sensitive to the noise than others and so no psychic powers may be may be manifestated from within this radius.

Roll a 1d3 to determine how many battle rounds the screaming lasts and place a die in the hex field to count down the remaining battle rounds.

##### 65: Trapped Blackstone Market Trader

No matter which turn it is rolled on, this encounter happens at the end of the game. On its way out of the Fortress the winning Kill Team finds a Blackstone Market Trader trapped in some stone formation collapsed by a Fortressquake. The desperated merchant will offer the Kill Team very special trade conditions for it's rescue: The Kill Team can buy everything for the regular base price in a next single trading phase but the amount of goods (units/wargear) is limited to 1d3.

##### 66: Unit event: The Dreaded Ambull

The Kill Teams encounter the most deadly creature of the Fortress (which is known yet): The Dreaded Ambull appears in a randomly chosen hex field, tunneling out of the ground and will seek close combat as soon as possible with the nearest unit!

Profile	M	WS	BS	S	T	W	A	Ld	Sv	Max
Ambull	6	3+	6+	6	6	7	4	6	3+	1

This model is armed with enormous claws (Range: Melee, Type: Melee, S: User, AP: -3, D: 1d3, Abilities: -).

- Rad-Maggot Symbiosis: At the start of each battke round this model gains 1d3 lost wounds.

- Fearsome Beast: The Ambull causes fear.

- The Ambull is immune to all psychology tests, will never become Broken or Shaken.

Rill another 1d3: 1: Nothing happens, 2: the Ambull is flanked by 1 Borewyrm Infestations, 3: the Ambull is flanked by 2 Borewyrm Infestations.

Profile		M	WS	BS	S	T	W	A	Ld	Sv
Borewyrm	4	4+	6+	3	4	2	3	5	4+

This model is armed with vicious jaws (Range: Melee, Type: Melee, S: User, AP: 0, D: 1, Abilities: Each time you make a wound roll of 6+ for this weapon, that hit is resolved at AP -2 instead.).

- Borewyrm Infestations are harder to target with ranged weapons by virtue of their size so that all hit rolls are resolved with -1.

All units wounding the Ambull successfully will receive +1 EXP after the battle and the unit killing the Ambul gets an additional +1 EXP. If the Ambul is killed, place an objective marker at it's last position. The Kill Team controlling this objective marker at the end of the game will find an additional +1 ArcheoTech (from a unit or whatever the Ambul scuffed int the last days).

Designer's note: Based on the Ambull rules for Kill Team released in White Dwarf Nov 2019. https://www.warhammer-community.com/2019/02/14/14th-feb-the-dreaded-ambull-in-warhammer-40000gw-homepage-post-3/ would be an alternative would be an alternative with "Rapid Tunnelling" as specilisation.

#### TODO Beware of witty warp effects

- "Launen der Magie" für PSYKER hinzufügen

Designer's note: This section is based on a Mordheim supplement rules which the author couldn't find in English but just in the german version of White Dwarf 55 called "Launen der Magie" ("moods of magic"?), p60.)

#### Maraud the battlefield

##### ArcheoTech founds

For each (partial) 15 points of the sum of all yout Kill Team units which are not taken out of action during the battle and which are able to move (so no turrets etc.) roll 1d6 to check how mich ArcheoTech you found:

Dice Result	 	ArcheoTech Founds
1-5				1
6-11			2
12-17			3
18-24			4
25-30			5
31-35			6
36+				7

+1 ArcheoTech for any objective marker you occupied in the mission "Territorial Imperative"
+1 ArcheoTech if you won the mission "Territorial Imperative"
+1 Hidden Vault Datacube (HVD) if you won the mission "Hidden Vault Datacube Hunt". If you have 4 VHD you can proceed as described in mission "The Hidden Vault" on the next day.

Example: A T'au Empire Kill Team starting with 125p survived a mission loosing 3 Drones with a total point value of 21p and an active Turret for 5p so the player rolls 7d6 (125-21-5=99, 99/15=6,6 -> 7d6).

Designer's note: This section is based on the Mordheim rule book (2004) pg99.

##### Exploration chart

If you had doubles (two 1/2/3/4/5/6s), triples (three 1/2/3/4/5/6s) etc. checking for ArcheoTech Founds, you've found something special in addition:

- any Doubles: +1 AT
- any Triples: +2 AT
- any Four of kind: +3 AT
- any Five of kind: +4 AT
- any Six of kind: +5 AT, if "666666": +1 HVD

Means: The results are cumulative, so one Double and one Triple means +3 AT.

Designer's note: This section is based on Mordheim rule book pg94 and had events like "you found a Noble's Villa" (on "666666" ) with the possibility to find special items and weapons. At time of writing the first version of this campaign rules I wanted to avoid balancing issues so I reduced the exploration chart to a possibility get "just" more money. In a later step a Kill Team could find here special ArcheoTech boosting profile stats.

### Back in Precipice

#### Checking the health of your units

Designer's note: This section is based on the Mordheim rule book (2004) p81.)

After the battle is over, both players work their way through the following sequence. Any dice rolls must be seen by both players or a neutral third party.

##### Physical consitution and Serious injuries (1d66)

Roll for all units which got flesh wounds during the mission 1d6 and substract -1 for each flesh wound the unit got:

- <=2: The unit suffered a serious injury and has to roll on the table for serious injuries.
- 3-6: The unit recovers fully.

All units which suffered a serious injury or were taken out of action during the mission have to roll 1d66 on the following table to determine the extent of their injuries before the next game.

Designer's note: This section is based on the Mordheim rule book (2004) p79-80.

###### 11-15: Dead

The unit is dead and the corpse will be giveen over to the space, never to be found again.

If a unit dies note the killed unit as "KIA" (Killed In Action) in your Command Overview (keep the unit on the command roster so that the CMC ID can not be taken a second time) and cross out the according Curriculum Mortis Card.

If the unit got out of action during the battle, all wargear of the unit is lost.

If the unit got out of action in Precipice, all residudes of the unit can be added to your spare part storage (note it under "Spare Part Storage" in your Command Roster.

###### Death of a LEADER

If the leader of the Kill Team is slain or there is no other leader on the your Command Roster:

- Case 1: If there is a COMMANDER this unit will become leader as deputy starting with leader level 1 in addition (as long as the COMMANDER is part of your Command).

In any other cases the leader will be chosen in the following way:

Case 2: If a unit (either specialist or non-specialist) has the insanity "Et tu, Brutus?" this unit will become the new leader. If two or more units have this ability they will fight against each other in the Arena in "Fear no Death" mode and the winner will become the new leader.

Case 3: The unit with the highest experience which is
- not already a specialist
- able to become a Leader
- able to get experience (so no Drones, Turrets etc.)
will become the new leader starting with leader level 1. If this fits to multiple units choose one by chance.

Case 4: If there is *no* non-specialist unit left on your Command Overview (and only in this case) a specialist unit must be chosen instead by chance and will become Leader. This specialist unit will become the specialism "Leader" *in addition* to it's original specialism and can level-up both specialisms but counts as 2 specialists in a Battle-forged Kill Team.

Case 5: If your current Leader can just not take part in the next mission(s) the described alternatives are temporary until your original leader is ready again (the debuty Leader will loose the "Leader" specialism then).

Case 6: If there is neither a non-specialist which could become Leader nor a specialist which could become Leader left, a unit which would normally be not able to become a specialist will become one (yes even a Drone... the power of the Blackstone Fortress, you know).

###### 16-21: Multiple injuries

The unit is not dead but has suffered a lot of wounds. Roll 2d3 times on this table. Re-roll any ‘Dead’, ‘Captured’ and further ‘Multiple Injuries’ results.

###### 22: Leg wound (or whatever makes the unit mobile)

The unit’s leg is broken. He suffers a -1 Movement characteristic penalty from now on.

###### 23: Arm wound (or whatever holds the weapon of the unit)
 
Roll again 1d6:

- 1: Severe arm wound. The arm must be amputated. The unit may only use a single one-handed close-combat weapon or a pistol as range weapon from now on.
- 2-6: Light wound. The unit must miss the next game.
 
###### 24: Madness

The unit gets +1 INS.

In addition roll 1d6:

- 1-3: The unit suffers from STUPIDITY from now on.
- 4-6: The unit suffers from FRENZY from now on.

###### 25: Smashed Leg (engine etc.)

Roll again 1d6:

- 1: The unit may not advance any more but may chill charge.
- 2-6: The unit has to recover the rest of the current and the next ingame day.

###### 26: Chest Wound

The unit has been badly wounded in the chest. He recovers but is weakened by the injury so his Toughness is reduced by `-1`.

###### 31: Blinded in one Eye

The unit survives but loses the sight in one eye (50% of optical sensors etc.); randomly determine which with 1d6: 1-3 left, 4-6 right eye. A unit that loses an eye has his Ballistic Skill reduced by -1. If the unit is subsequently blinded in his remaining good eye he must retire from the Kill Team if

- the unit is not a PSYKER which could still use PSYKER attacks but no other ranged or close combat attacks and
- it stays in 2" of a friendly unit the whole time which works as guide (without a guide if the unit wants to move it has to move in a random direction with a normal move and can get into close combat this way).

###### 32: Old Battle Wound

The unit survives, but his wound will prevent him from fighting if you roll a 1 on a 1d6 at the start of any battle. Roll at the start of each battle from now on.

###### 33: Nervous Condition

The unit gets +1 INS and the unit’s nervous system has been damaged: The Ld is permanently reduced by `-1`.

###### 34: Hand Injury

The unit’s hand is badly injured. His Weapon Skill is permanently reduced by -1.

###### 35: Deep Wound

The unit has suffered a serious wound and must miss the next 1d3 games while he is recovering. He may do nothing at all while recovering.

###### 36: Robbed

The unit managed to escape, but all his wargear is lost and belongs now to the opponent Kill Team which took the unit out of action. Note that "wargear" is only something that has dedicated entry in a Kill Team's "Weapon Profiles and Points" list.

###### 41-55: Full Recovery

The unit has been knocked unconscious, or suffers a light wound from which he makes a full recovery.

###### 56: Bitter Enmity

The unit makes a full physical recovery, but is psychologically scarred by his experience. From now on the unit hates the following (roll a D6):

1d6	Effect
1-3	The individual who caused the injury.
4	The leader of the Kill Team that caused the injury.
5	The entire Kill Team of the unit responsible for the injury.
6	All Kill Teams of the same faction.

###### 61: Captured

The unit regains consciousness and finds himself held captive by the other Kill Team.

The unit may be ransomed at a price set by the captor or exchanged for one of their Kill Team who is being held captive.

Captives may be sold at the Blackstone market. Chaos factions may sacrifice the prisoner. The leader of the Chaos Kill Team will gain +1 Experience if they do so.

It is up to the negotiating leaders if captives who are exchanged or ransomed retain all their wargear; if captives are sold or killed their wargear is retained by their captors. Note that "wargear" is only something that has dedicated entry in a Kill Team's "Weapon Profiles and Points" list.

###### 62-63: Hardned

The unit survives and becomes inured to the effects of the Fortress. The unit looses the effect of a randomly chosen insanity effec. If it has no one yet but will maybe get one *on the same ingame day* (Arena etc.) it will not get the next effect, neither positive nor negative.

###### 64: Horrible Scars

The unit causes FEAR from now on.

###### 65: Sold to the Pits of the Servants of the Abyss

The unit wakes up in the infamous fighting pits of the Servants of the Abyss and must fight against an Ur-Ghul according to the KTonBSF rules.

If the unit loses, roll to see whether he is dead (so "Killed in Action") or injured. If he is not dead, he is thrown out of the fighting pits without all wargear and may re-join his Kill Team. Note that "wargear" is only something that has dedicated entry in a Kill Team's "Weapon Profiles and Points" list.

If the unit wins it manages to escape and will rejoin his Kill Team with all wargear, 1d3 ArcheoTech and +2 EXP.

If there is a Servants of the Abyss playing participant the player may decide to take the unit as hostage or let one of its units let fight against the captive unit. In case the SotA unit wins it gets (in addition to campaign Arena rewards) +1d3 EXP for delighting Obsidius Mallex. If there are 2 or more SotA players each of them interested in the hostage has to roll 2d6, the highest roll wins the hostage.

###### 66: Survives against the Odds

The unit survives and rejoins his Kill Team. The unit gains +1 Experience.

##### Mental constitution / Insanity (INS) (4d6)

The ancient power of a Blackstone Fortress will break the mind of even the hardest unit or will twist even creatures of chaos in a new way.

All units will get Insanity points in the following way:

- +1 for any day a unit has survived in general (even if it were not part of a mission because Precipice is too close to the Fortress not to be affected by it)
- +1 for participating in a mission in the Blackstone Fortress

Roll 4d6 and see under which insanity effect the unit will suffer. Note any effect on the back side of the CMC of the unit in the "Level-up effects log" column "Insanity":

###### 4: Amnesia

The unit forgets/loses 1d3 random abilities.

###### 5: "Temper, temper!"

The unit has a serious temper problem. Roll 1d6 before each turn and on a score of 1 the unit is sulking and may do nothing else this turn, except defend himself the unit is in close combat. If the unit is charged during this time, the unit is FRENZY for the current battle round.

###### 6: Chosen by the Fortress

The unit wonders if it is chosen by the Fortress as the so called "Prophet of the Blackstone". Although it is not completely convinced yet it is obsessed by it's own (secret) mission finding *the Blackstone*which will proove that the unit is the chosen one. The unit will not support the Kill Team finding ArcheoTech because it is so obsessed finding it's personal relic.

###### 7: Paranoia

The unit is always on edge and thinks someone is out to get him. The unit may not advance but may charge into combat as normal. The unit will always hit first in close combat.

###### 8: "What a cute and fluffy little thing!"

The unit refuses to attack units with a lower point value.

###### 9: Ministry of Funny Walks

The unit is convinced that if walking in a bizarre manner other units won't be able to hit it with ranged weapons. The unit may not advance but it is at -1 to hit with ranged weapons as the unit is weaving all over the place.

###### 10: Criminal Psychosis

The unit gains ANIMOSITY.

###### 11: Hallucinations

The unit suffers from horrible hallucinations. Roll 1d6:

- 1: The unit sees nightmare images of his own demise, filling him with fear for his own safety. No matter how far away the unit is from friendly models it will never profit from Ld bonuses of friendly models.

- 2: The unit hallucinates and sees an image of what is yet to come. The unit may re-roll a single shooting or close combat roll to hit (even a re-roll).

- 3: A faint imagine of the unit's personal god appears, be it the Omnissiah or Tzeentch. Filled wiht courage, the unit may ignore any and all Ld based tests the unit is required to make.

- 4: The unit peers into the depths of it's own mind, unlocking untapped abilities. The unit can detect perfect line of sights of all units it can see and passes on the information to it's compatriots withing 3" so that the affected enemy units will loose do not profit from being in cover anymore.

- 5: The unit hallucinates that a slender arm reaches out from the darkness around him, and pale fingers touch the warrio's chest. The soft caress sends shivers of terrible pain throughout the unit's body. Though it quicly dims away, the terrible pain is left behin. The next wounding hit the unit receives is automatically a critical hit.

- 6: The unit hallucinates that the Blackstone Fortress chooses to reveal its secret to the unit, unveiling the ancient intelligence that lurks behind the the mystic architecture of the Fortress. The unit's mind is overwhelmed by the enormity of the impression and the unit stumbles away in stark terror. For as long as this insaity lasts, all enemy models count as causing fea and the unit will refuse to get within 2" of any walls or ruins. After the unit has recovered from this insanity the effects will wear off, though the unit will get "Claustrophobia" indefinitely (see effect 12).

###### 12: Phobia

The unit is cused with the following a phobia. Roll 1d6:

- 1: Ochlophobie

The unit must take a feat test before entering or staying in a place where 2+ other units would be within 3".

- 2: Aichmophobie

The unit must pass a fear test before attacking units wearing spiky items (wargear like swords, chaos spikes on armor, these very long thin Aeldari sniper rifles etc.).

- 3: Claustrophobia

The unit must pass a fear test before trying to use the rule "Edging through bottlenecks".

- 4: Hypnotopophobie

The unit fears to go to sleep. At the end of each day roll 1d6 for the unit. On 4+ the unit refuses to sleep and is affected by -1 on hit rolls and -1 on Ld tests for the whole day.

- 5: Coulrophobie

The unit fears HARLEQUINS from now on.

- 6: Miaphobia

The unit fears the fears the number 1 and can re-roll all the unit's dice rolls of 1 if directly related to the unit (hit rolls, wound rolls, save rolls, LD tests etc.). If the unit has already an ability allowing to re-rell special dice rolls of 1 the unit can re-roll such rolls a second time.

###### 13: Heroic Idiocy

The unit may never end it's movement phase behind cover. The unit is immune to psychology and receives +1 EXP if it is still standing at the end of a battle.

###### 14: Panzaism/Quixitism

- 1-3: Panzaism: The unit is immune to FEAR as he sees all things (even supernatural) as ordinary.
- 4.6: Quixitism: The unit sees eberything and everyone as supernatural threats. The unit is FRENZY.

###### 15: "Et tu, Brutus?"

If the unit is ever the only model within 6" of the leader the unit will charge the leader and try to kill him so the unit can take over the Kill Team. If the unit is the Kill Team's leader, then ignore this result and roll again on this table.

###### 16: "I hate you all!"

The unit is subject to HATRED to all enemy units and ANIMOSITY to friendly unit'S.

###### 17: Catatonia

The unit falls to the floor and rolls into the fetal position. The unit has to be treated as described under STUPIDITY.

###### 18 I can (not) fly (anymore)!

If the affected unit can *not* FLY it *can* FLY now (yes, the Blackstone Fortress can change physical laws).

If the affected unit *can* FLY it can *not* FLY anymore (so the only way to move will be by foot - if the unit has no feet (Drones etc.) it can not move anymore).

###### 19 Tis but a mere flesh wound

The unit gains an extra Wound.

###### 20 Prometheum addiction

The unit drowns it's fears and sorrows by consuming Prometheum. The unit suffers -1 on WS and BS as long the Prometheum addiction lasts and the need for Prometheum will costs the Kill Team 2d6 BlackCoins per inagme day.

###### 21 Multiple Personalities

The unit thinks to be a(n):

| 1d20 | Result/Effect | 
| --- | --- |
| 1 | Adeptus Astartes Tactical Marine (core rules pg84) |
| 2 | Deathwatch Veteran (core rules pg90) |
| 3 | Grey Knight (core rules pg94) |
| 4 | Astra Militarum Infantry Squad Guardsman (core rules pg101) |
| 5 | Adeptus Mechanicus Skitarii Ranger (core rules pg108) |
| 6 | Heretic Astartes Chaos Space Marine (core rules pg106) |
| 7 | Death Guard Plague Marine (core rules pg122) |
| 8 | Thousand Sons Rubic Marine Aspiring Sorcerer (core rules pg128) |
| 9 | Asuryani Guardian Defender (core rules pg135) |
| 10 | Drukhari Kabalite Warrior (core rules pg144) |
| 11 | Harlequin (core rules pg148) |
| 12 | Necron Warrior (core rules pg154) |
| 13 | 1-3: Ork Boy, 4-6: Gretchen (both core rules pg128) |
| 14 | 1-3: T'au Fire Warrior Shas'la (core rules pg170) |, 4-6: Kroot (elite rules pg107) |
| 15 | Tyranid Genestealer (core rules pg179) |
| 16 | Genestealer Cult Acolyte Hybrid (core rules pg186) |
| 17 | Elucidian Starstriders Voidsman (rogue trader rules pg32) |
| 18 | Gellerpox Infected Nightmare Hulk (rogue trader rules pg40) |
| 19 | Daemon Horror (White Dwarf 2019-07 pg137) |
| 20 | Adeptus Custodes Custodian Guard (elite rules pg58) |

Re-roll if the result shows something that the unit already is.

Use the stats and the abilities for the unit of that race instead of the unit's normal stats and abilities. Wargear stays the original one except for Greenskins (we know: if an Ork thinks that a stick is a Plasma rifle the stick will behave like a Plasma rifle).
Furthermore the unit can use all abilities given by keywords, for example: PSYKER will make a Non-PSYKER a PSYKER this way). The unit can even use the tactics of the other faction.

###### 22 Whose side am I on anyway?

Ath the beginning of each battle round each side must roll a dice. The player of the insane unit may add 2 the roll. The player with the highest roll controls the confused unit for the battle round and the unit may be treated as enemy unit by it's own Kill Team in this case.

###### 23 Megalomania

The unit must move to the nearest enemy unit and may not end its movement behind an obstavle or in hiding. The unit is not effected by SHAKEN units or BROKEN status of its kill team.

In addition, if the unit is the LEADER of the kill team the unit cannot take decide for an strategical withdrawal as lof as this insanity lasts.

If the unit survives the end of a battle it gains +2 additional EXP.

###### 24 Completely bonkers

The unit gets 2d2 insanity effects. Roll again and ignore further results of 24.

Designer's note: This section is based on the Mordheim rules "At the Mouth of Madness" by Donato Ranzato et al. (no idea when or where presented for the first time).

#### Experience (EXP) gain

In difference to the core rules section "Experience" on p204-205 units gain experience in the following way:

Any unit:

- +1 for surviving a mission
- +1 for each kill 
- +1 if the unit killed a specialist
- +1 if the unit killed a specialist which was a leader or COMMANDER

Specialists:

- +1 if used at least one of your specialist tactics during the last mission

Note any experience point on your CMC and gain a level up for any grey field according to the core rules pg205 with the following difference:

Completely ignore the following sub sections of the "Experience" section:

- Fire Teams
- Guerilla Factions
- Sudden Death
- Escalatation

##### Level-ups for Specialists

If a specialist unit gets a level-up you may choose if they level up either like non-specialists (see next section) or you may choose or according to core rules pg66.

After becoming level 4 a specialist will level-up as a non-specialist in any case.

Some of the specialist level abilities of the core rules pg69 won't work in this campaign because here we don't use the ressources of the core rules pg202 ("Intelligence, Materiel, Morale and Territory"). For this reason all specialist level abilities giving a bonus on these ressources have to be adjusted. The adjustments are:

###### COMMS SPECIALISTS Lvl3: Vox Hacker (core rules pg70):

Replace

> Vox Hacker: After each battle in which this model was in your kill team, if this model is not in Convalescence (pg 204) or dead, roll a D6. On a 5+ you gain 1 Intelligence.

with

> Vox Hacker: Before each battle in which this model is part of your Battle-forged Kill Team, roll 1d6. On a 4+ you can choose your opponent (so there is no matching for you and this opponent, change your place with the former opponent of your new opponent). If 2 or more players choose the same opponent all have to roll 2d6 and the player with the hightes result can decide. On a 6 you can match 2 opponents on a certain map.

###### DEMOLITIONS SPECIALISTS Lvl3 (core rules pg71)

- **Saboteur**:

Replace

> Saboteur: If this model is in your kill team and not out of action when you make your Casualty rolls, roll a D6. On a 5+ choose an opponent who played that mission to lose 1 Materiel.

with

> Saboteur: If this model is in your kill team and not out of action when you are in the "Maraud the battlefield" phase you can manipulate 1d3 dice results of your opponents in this phase by `+/-1` (in case of multiple opponents in the mission the result shows the amount for all of your opponents, not just one).
Example: Given you played against 2 opponents, the first opponent rolled 4d6 with `3, 3, 3, 4` as result and the second opponent rolled 3d6 with `6, 6, 5` as result for AT founds. Your DEMOLITION rolled 1d3 with a 3 as result and can modify 3 dice by `+/-1` now. You decide to modify 2 dice of opponent `+/-1` (`3, 3, 3, 4` to `2, 3, 3, 5`) and 1 die of opponent 2 (`6, 6, 5` to `6, 6, 6`). It is up to you to modify in a positive or negative way (maybe one of the opponents became your ally and you want to support this team?).

- **Ammo Hound**:

Replcace

> Ammo Hound: If this model is in your kill team and not out of action when you make your Casualty rolls, roll a D6. On a 5+ you gain 1 Materiel.

with

> Ammo Hound: If this model is in your kill team and not out of action when you are in the "Maraud the battlefield" phase you can manipulate 1d3 of your dice results in this phase by `+/-1`.
Example: Given you rolled 6d6 with `5, 5, 5, 6, 6, 6` as result for AT founds. Your DEMOLITION rolled 1d3 with a 3 as result and can modify 3 dice by `+/-1` now. You decide to modify 3 dice by 1 and change `5, 5, 5, 6, 6, 6` to `6, 6, 6, 6, 6, 6`.

###### SCOUT SPECIALISTS Lvl3 (core rules pg74):

- "Observer:

Replace

> Observer: If this model is in your kill team, you can roll a D6 at the start of the Scouting phase. On a 4+ you can pick an additional strategy.

with

> Observer: If this model is in your Battle-forged kill team, you can re-roll the result for "Get a subplot" one time.

- Explorer:

Replace

> Explorer: After each battle in which this model was in your kill team, if this model is not in Convalescence (pg 204), you can roll a D6. On a 5+ you gain 1 Territory.

with

> Explorer: If this model is in your Battle-forged kill team, you can choose the mission in section "Get a mission". If 2 or more matches players use this ability roll 2d6 and the player with the highest result can choose the mission.

##### Level-up for Non-Specialists

1d6	Result
1:	Fleet: Add 1" to this model’s Move characteristic.
2:	Lucky: You can re-roll save rolls of 1 for this model.
3:	Courageous: You can re-roll failed Nerve tests for
this model.
4:	Long range/close combat skilled: Roll another 1d6:
	1-3 : You can re-roll hit rolls of 1 for this model when it makes shooting attacks.
	4-6: You can re-roll hit rolls of 1 for this model in the Fight phase.
	If the unit already has the one or the other it will get the missing ability instead.
5:	long range/close combat lethal: Choose one:
	1-3: You can re-roll wound rolls of 1 for this model when it makes shooting attacks.
	4-6: You can re-roll wound rolls of 1 for this model in the Fight phase.
	If the unit already has the one or the other it will get the missing ability instead.
6:	Die-hard: You can subtract 1 from Injury rolls for this model.

Average units became a specialist type on level 9 and you have to draw by chance which kind of specialist they become according to their profile - if there is already a unit in your Kill Team withe the drawn speciality draw by chance another one. If your Kill Team has any possible specialist of the unit type on its Command Overview you can have an additional one of the same type.

Each unit reaching level 12 gets +1 for a profile value of your choice (even one that has already been increased and/or usually could not be increased like +1 wounds) or, in case of specialists, can choose another specialist ability not taken yet!

Attention: Level ups (neither for regular units nor for specialists) do *not* increase the CPV of a unit (they will become insane very soo so "level up" is a strange term related to experience units get in the Blackstone Fortress).

All other rules of the core rules stay the same.

#### Mission Glory for your Kill Team

Designer's note: This section is based on the Mordheim rule book (2004) p81.

Modify your Geneal Glory count:

-+1 if you won the mission
-+1 if you won the mission and all opponent Kill Teams had a higher Mission Glory value than you Kill Team
- +2 if the other Kill Team(s) were completely taken out of action during the mission
- -1 if you lost the mission
- -1 if you lost the mission and all opponent Kill Teams had a lower General Glory value than your Kill Team
- +/-0 if the mission ended with a tie

#### Check your finances

##### Exchange ArcheoTech (AT) for BlackCoins...

Designer's note: This section is based on the Mordheim rule book (2004) p101 which tried 

Any Kill Team needs financiel ressources for ! For this puprpose they need "BlackCoins", the currency of Precipice, because this is the only currency accepted by local traders.

You do not have to sell all your ArcheoTech after the battle (you may want to hoard it or analyze it later). Unfortunately, the demands of running a Kill Team often mean that you will have to sell most of your ArcheoTech as soon as you find it. There are ususally 2 possibilities to do so and you have to decide which amount of your AT stock you want to sell the one and/or the other way if you want to sell it because there is only time for one sell the one and/or the other way.

###### ...At Black Rock - the bank of the Precipice Trader Union (PTU)

Black Rock always buys ArcheoTech for a substandard but fixed price. The more ArcheoTech you sell the more BlackCoins you will get:

AT	BlackCoins	Average
1	5			5/AT
2	12			6/AT
3	21			7/AT
4	32			8/AT
5	45			9/AT
6	60			10/AT
7	77			11/AT
8	96			12/AT
9	117			13/AT
10+	140			14/AT

###### ...on the Blackstone Market

Risky but maybe more profitabel is it to sell ArcheoTech on the Blackstone Market. You can solicit for a bid but if you refuse the offer you won't have neither time for another offer at Blackstone Market nor time to to sell the AT at Black Rock on the same day (you can come back for another offer after the next mission, if course - but keep in mind that you have to pay maintenance costs).

The offered BlackCoins are calculated the following way:

AT	BlackCoins	Average/Sell	Average/AT
1	3d10		15				7,5
2	3d10+1d6	18				9
3	3d20+1d3	~31,5			~10,5
4	4d20+4d3	48				12
5	6d20+2d6	~67,5			~13,5
6	9d20		90				15
7	11d20+1d10	~115,5			~16,5
8	14d20+2d3	144				18
9	17d20+1d10	~175,5			~19,5
10	21d20		210				21
+1	+6d6		+21				21

(if you don't have d20s take the double amount of d10s)

#### Trading phase (rearm your Kill Team, visit the "doctor", get bankrupt, have "fun" at the Arena etc.)

Trading is allowed at any time after returning to Precipice and before a new ingame day started (so you can still trade while and after Arena matches).

Keep in mind that you have to pay maintenance costs for your Kill Team at the end of the day (see section "Pay maintenance costs for your Kill Team").

Players can trade in any way they want among each other (ArcheoTech, Blackcoinsi, Prisoners, Contracts) with individual prices for selling loot, wargear or prisoners (some cruel leader may even want to sell units?).

##### Blackstone Market

Traders at the Blackstone Market only accepts BlackCoins for trading. The Blackstone Market is usually the only way to buy wargear or smuggle supplies to Precipice for the following (horrible high) prices:

- Buying:

You can only buy things has dedicated entry in a Kill Team's "Weapon Profiles and Points" list.

1) A **new unit** costs the unit's total point value as base price + 1d6 for every 5 points of the total point value starting to count the steps by 0, not 1 (so `0-4` = +1d6, `5-9` = +2d6, ...) in BlackCoins.

Example: A Tactical Marine Gunner (13p) with a Grav-gun (2p) would cost 15+4d6(~14)=29 BlackCoins. The point value of the unit including wargear stays the default one.

2) To calculate price for buying just a **wargear** of a certain unit is calculated as described in 1) but the *base* price (=*total* point value) is divided by 2 (rounded up) + Xd6 according to the point value of *just the item* you want to buy.

Example: A Grav-gun (2p) for a Tactical Marine Gunner (13p) would cost 15/2(=8)+1d6(~4)=12 BlackCoins.

New units don't need to participate directly in a battle. Ignore the "New Recruit" rule of the core rules pg205.

- Selling:

The price for selling something is calculated as described in 2) but the final prive is divided again by 2 (rounded down), example: A Grav-gun (2p) of a Tactical Marine Gunner (13p) could be sold for (13/2(=7)+2+4d6(=14))/2=11 BlackCoins.

Important: If you decide to sell a member of your Kill Team (which means everything which can gain experience) you will loose the ability to call your faction for emergency reinforcements for the rest of the campaign, traitor!

##### TODO Need a Painboy?

Cost: like buying the unit on Blackstone Market (but without extra wargear)

Designer's note: -> mordheim - towncryer08 - sawbones, pg34

##### TODO Analyze your ArcheoTech

The ArcheoTech found by Kill Teams and Adventurers in the Fortress can be very powerul auxiliary equipment but in most of the cases the knowledge how to use it - if it's not already broken laying in the dust over centuries - is lost. Fortunately there are specialists in Precipice able to analyze ArcheoTech and maybe find out the original purpose. Back in Precipice a Kill Team can let check their ArcheoTech at the market.

Designer's note: -> Mordheim, power to the stones

##### Pay maintenance costs for your Kill Team

The more models there are in your Kill Team, the more it costs to maintain. The maintenance costs of all your units (prisoners), wargear and all the other stuff needed in your Command for the current ingame day is the Total Point Value divided by 5 (rounded up) in BlackCoins.

Skip this section if you just return from your *first* mission (your Kill Team arrived in Precipice with enough ressources to pay maintenance costs for the first day).

Example: A Kill Team of 3 Tactical Space Marines (3x12=36p), 1 Tactical Space Marine Gunner with a Grav-Gun (13+2=15p), 1 Tactical Marine Sergeant (13p), 1 1 Chaos Cultis (4) as prisoner has a Total Point Value of 36+15+13+4=68. So the maintenance costs are 68/5=14.

The costs represent a fee that has to be payed for accomodation, energy costs und usage of general technical facilities to maintain your wargear.

You have to pay theses costs at the end of the day before the Arena fights by substracting the value from your BlackCoins balance.

###### Out of money? Welcome to the bounty hunt - you are the target!

If you can not pay the fee the debtees will directly inform the Rogue Trader Union which is the autonomy of Precipice which will set a bounty on

- (all) your Leader(s)
- a randomly chosen unit of your Kill Team for any (partial) 10 BlackCoins you own the Roge Trader Union.

Example: If there is a Kill Team with 2 Leaders having debts of 15 BlackCoins the Rogue Trader Union will set a bounty on the 2 Leaders and 2 randomly chosen units of your Kill Team.

All units wanted will be published on a wanted poster with the following information:

```
WANTED

Since day:
Unit name:
Model type:
CMC ID:
Kill Team name:
Player name:
```

Rewards:

- The CPV of the unit in BlackCoins if your Kill Team takes the wanted unit out of action in a mission like described in "Bounty Hunters" in arena rules pg21.
- Additional CPV of the unit in BlackCoins if the wanted unit died after the mission.
- Additional CPVx2 of the unit in BlackCoins if the wanted unit is captured by your Kill Team and handed over to the RogueTraderUnion.

The wanted status will be dropped if the debts are payed with a fine of 5 BlackCoins another 5 BlackCoins for any day passed since the bounty was announced.

##### Calling for emergeny reinforcements

If the Mission Glory of your Kill Team is -3 behind the Kill Team with the leading Mission Glory you can call your faction command for emergeny recinforcements. A randomly chosen COMMANDER of your faction will join your Kill Team.

If you fall back to -4 or more Mission Glory behind the Kill Team with the leading Mission Glory even with the support of the given COMMANDER  you can can call for another emergency reinforcements but are allowed to *choose* the COMMANDER you want this time.

All COMMANDERs does not count to the current max. points or units value for Battle-forged Kill Teams (but you can only take 1 COMMANDER with you, of course, but yes, you coud have an 21 unit Kill Team with the value of 200+ points this way). COMMANDERs does not cost maintenance after the mission and will stay as long as your Kill Team's Mission Glory has the same value as the leading one after a mission. In this case all COMMANDERs will leave your Kill Team.

Designer's note: This section tries to integrate COMMANDERs in the campaign setting without rewriting existing rules.

##### At the end of the (ingame) day: Knocking-off (or out) at Holofrag - the Arena of Precipice

At the end of each ingame day any participating Kill Team can send a unit into the Arena of Precipice.

Designer's note: This is somehow an optional part to have a "side plot". There was a try to implement a betting system but this was to time consuming during the game.

###### Registration

Any player can register one single Non-COMMANDER unit for the Holofrag match. A registration costs 50 Cent and the player has to choose the fight mode:

- "Holo" mode ("H"): The unit will not collect any glory points but won't suffer any effects and injuries after getting out of action in any way.

- "Fear no Death" mode ("D"): The unit will suffer all effects and injuries like in a Kill Team mission according to section "Check your units" but will collect glory points and receive extra experience.

Note in your CMC the mode ("H" or "D") you want to participate in the according field for "Arena Mode" and place it in the registration box next to the Arena so that nobody can see which unit you have registered. Once placed you can not withdraw your participation.

It is allowed to challenge a certain Kill Team unit (note this with a "C" behind "H" or "D" followed by the CMC ID of the challenged unit on your CMC and announce the challenge in presence of more than a half of all participating players). If the player of the Kill Team accepts both are matched automatically. If the challenged player refuses the challenger can decide to withdraw the registration or participate in another mode.

###### Matching the participants

Write all participants on a card and draw always two by chance to match the participants.

If the number of participants are odd-numbered 3 participants will fight against each other in the same match.

###### Ready? Fight!

The matched participants fight according to "Warhammer 40,000: Kill Team Elites" (2019, updated by errata v2019-08-23) rule book, updated by errata v2019-08-23 on the prepared battlefield.

In case of of 3 participants not the regular start positions are used but random selected ones using m anerrance die and 3d6" distance from the middle of the map (in a way that no participant is in close-combat with another participant from begin on).

Only a LEADER has a single Command Point for the whole arena fight.

An arena fight lasts max. 6 rounds. After each battle round the arena field shrinks by 1 square in circumference (compare the squares printed on the field), so in the last round the outer walls of the arena are 5 squares closer to the middle of the arena.

###### Achieve experience

- +1 for participating
- +1 for winning
- +1 if you killed a leader
- +1 for each additional kill
- +2d3 if participated in "Fear no Death" mode

###### Achieve glory points

Holo mode:

- +1 for winning
- -1 for loosing

"Fear no Death" mode:

- +1 for participating
- +1 for winning
- +1 for any suffered flesh wound
- in case you won: +1 for every 5 points your opponents point value is higher than your own (example: if your's is 15 and your opponents is 21 you will get `+2`)
- -1 for loosing
- in case you lost: -1 for every 5 points your opponents point value is lower than your own (example: if your's is 15 and your opponents is 21 you will get `+2`)

# Final designer's notes

## Thx

Despite all former and current Games Workshop employees for the greates hobby universe existing I want to thank the following people in alphabetical order for their helpful inputs and comments planning: Ali, Duc, Filip, Kilian, Nuri, Philipp and Robert!

## General motivation

Coming from former GW skirmish games the official Kill Team campaign rules are quite disappointing compared to the campaign experience old skirmish games like [Mordheim](https://en.wikipedia.org/wiki/Mordheim) provided. So my intention with this rule set is to combine the modern skirmish rules of Kill Team with campaign rules designed for the very narrative campaign experience of "flavored" by [Shadow War: Armageddon](https://www.warhammer-community.com/2017/03/15/war-returns-to-armageddon/) here and there.

The key for a Mordheim like feeling in 40k is to play games in an atmospheric environment similar to the one Mordheim provided: For me this is the dark, arcane and narrow maps of Blackstone Fortress.

The campaign has a very narrative focus and is not competitive at all because a lot of events with a very big impact will affect all campaign participatns very accidentally. 
