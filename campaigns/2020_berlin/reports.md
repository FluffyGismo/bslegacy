**Blackstone's Legacy 2020 Berlin campaign report**

## Campaign days

| Date | Time | Played Ingame Days (IgDay) |
| --- | --- | --- |
| Sa, 11th Jan 2020 | 11am to 7pm | IgDay01 |
| Sa, 1st Feb 2020 | 11am to 7pm | ? |
| Sa, 7th Mar 2020 | 11am to 7pm | ? |
| Sa, 4th Apr 2020 | 11am to 7pm | ? |
| Sa, 2nd May 2020 | 11am to 7pm | ? |
| Sa, 27th June 2020 | 11am to 7pm | ? |  

- Org. date vote: https://poll.digitalcourage.de/ZrVZU1CZmDPgdJ7x

# Participants (in alphabetical order)

| Player name | Faction | Kill Team name | Kill Team abbreviation | Participation on IgDays |
| --- | --- | --- | --- | --- |
| Ali | Necrons | FluffyGismos | FG | 01, 02 |
| Duc | Drukhari | No Pain No Gain | NPNG | 01 |
| Filip | Death Watch | Garbage Collection | GC | 02 |
| Julian | Orks | Lucky Looters | LL | 01, 02 |
| Kilian | Death Guard | Lepra-Express | LPX | 02 |
| Nuri | Catachan Ork Hunters (Genestealer Cults rules) | ? | ? | 02 | 
| Philipp | Adeptus Astartes | Soriel's Battlegroup | SB | 01, 02 |
| Roland | T'au Empire | For the Greater Legacy | FtGL | - |

# Ranking

## Team ranking

### Blackstone Fortress missions

| Kill Team abbreviation | Mission Glory |
| --- | --- |
| FG | 1 |
| GC | 1 |
| LL | -1 |
| NPNG | - 1 |
| SB | -1 |

### Precipice Arena matches

| Kill Team abbreviation | Arena Team Kills | Arena Team Glory |
| --- | --- | --- |
| FG | 2 | 5 |
| SB | 1 | 1 |
| GC | 0 | 0 |
| LL | 0 | -1 |
| NPNG | 0 | -2 |

## Single ranking (Arena plot only)

| Character name | CMC ID | Sub-Faction | Arena Unit Kills | Arena Unit Glory |
| --- | --- | --- | --- | --- |
| Eknotath | FG02 | Novokh | 2 | 5 |
| Ezekial Kaelon | SB03 | Dark Angels | 1 | 1 |
| ? | GC07 | - | 0 | 0 |
| Boltz | LL07 | Deathskulls | 0 | -1 |
| Edward | NPNG01 | ??? | 0 | -2 |

# Mission reports

## Ingame Day 01 (2020-01-11)

### Blackstone Fortress missions

#### Summary

##### "Soriel's Battlegroup" (SB) VS "Garbage Collection" (GC)

[![bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg "BSLegacy ingame day 01 - Soriel's Battlegroup VS Filip's Space Marines")](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut.jpg)

Result: GC won

##### "FluffyGismos" (FG) VS "NoPainNoGain" (NPNG) VS "Lucky Looters" (LL)

[![bslegacy2020-01-11-berlin_igday01_11-46-42_cut400](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut400.jpg "BSLegacy ingame day 01 - FluffyGismos VS No Pain No Gain VS an Ork Kill Team")](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut.jpg)

Result: FG won

### Precipice Arena matches

#### Summary

- "Ezekial Kaelon" (SB03) won against "Boltz" (LL01)
- "Eknotath" (FG2) won against "Edward" and ? (GC07)

## Ingame Day 02 (2020-01-25)

### Blackstone Fortress missions

#### Summary

##### "Soriel's Battlegroup" (SB) VS (Nuri)

##### "FluffyGismos" (FG) VS "Lucky Looters" (LL)

##### "Garbage Collection" (GC) "Lepra-Express" (LPX)

### Precipice Arena matches

#### Summary
