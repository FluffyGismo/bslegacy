# Team ranking (main and arena plot)

| Player name | Kill Team name | Kill Team abbreviation | Faction | Mission Glory | Arena Team Glory |
| --- | --- | --- | --- | --- | --- |
| YourName | YourKillTeamName | YourKillTeamAbbr | YourKillTeamFaction | YourMissionGlory | YourArenaTeamGlory |

# Single ranking (arena plot only)

| Character name | CMC ID | Arena Kills | Arena Unit Glory |
| --- | --- | --- | --- |
| YourCharName | YourCharNameCMCID | YourCharsArenaKills | YourCharsArenaUnitGLory |

